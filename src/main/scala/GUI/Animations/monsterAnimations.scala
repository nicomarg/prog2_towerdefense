package GUI
package Animations

import swing._
import java.awt.Color

class PegasusAuraAnimation(val radius:Double, val source:GameLogic.Monsters.Pegasus) extends Animation
{
    var clock = 0
    def paint(g: Graphics2D): Boolean = 
    {
        val src = source.pos + GameLogic.Position(0.5,0.5)
        val image = TowerDefense.getImage("/aura/".concat((((clock/4)%4)+1).toString.concat(".png")))
        val printx = (src.x- radius/2)*GridDisplay.MonsterDisplayer.cellsize.width
        val printy = (src.y- radius/2)*GridDisplay.MonsterDisplayer.cellsize.height
        g.drawImage(image,printx.toInt,printy.toInt,(radius*GridDisplay.MonsterDisplayer.cellsize.width).toInt,
            (radius*GridDisplay.MonsterDisplayer.cellsize.height).toInt,null,null)
        clock += 1
        false
    }
}

class UnicornHealAnimation(val radius:Double, val source:GameLogic.Position) extends Animation
{
    var clock = 0

    def clocktoFrame():Int =
    {
        if (clock < 20) clock/5
        else 4
    }

    def paint(g: Graphics2D): Boolean =
    {
        if (clock > 35) return true
        val src = source + GameLogic.Position(0.5,0.5)
        val image = TowerDefense.getImage("/Heal/".concat(clocktoFrame.toString.concat(".png")))
        val printx = (src.x- radius/2)*GridDisplay.MonsterDisplayer.cellsize.width
        val printy = (src.y- radius/2)*GridDisplay.MonsterDisplayer.cellsize.height
        g.drawImage(image,printx.toInt,printy.toInt,(radius*GridDisplay.MonsterDisplayer.cellsize.width).toInt,
            (radius*GridDisplay.MonsterDisplayer.cellsize.height).toInt,null,null)
        clock += 1
        return false
    }
}