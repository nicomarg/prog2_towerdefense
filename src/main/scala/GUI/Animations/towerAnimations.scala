package GUI

package Animations
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import swing._
import java.awt.Color
import java.awt.BasicStroke

abstract class Animation
{
    def paint(g:Graphics2D):Boolean
}

/**
  * A small animation for the tower shots
  *
  * @param source the origin of the shot
  * @param target the destination of the shot
  */
abstract class TowerFireAnimation extends Animation
{
    val source:Point
    val target:Point
    val norm = Math.sqrt((target.x - source.x)*(target.x - source.x) + (target.y - source.y)*(target.y - source.y))
    def position:Point = new Point(source.x + ((target.x - source.x)*speed*clock/norm).toInt, source.y + ((target.y - source.y)*speed*clock/norm).toInt)
    val speed = 10
    /** duration of the shot */
    val duration:Int = 15
    /** The number of frames elapsed */
    var clock:Int = 0
    val prefix:String

    /** a fonction that draws the bullet itself */
    def drawBulllet(g: Graphics2D) =
    {
        val backup = g.getTransform()
        val tx = target.x
        val ty = target.y
        val sx = source.x
        val sy = source.y
        val image = TowerDefense.getImage(prefix.concat((clock%duration).toString.concat(".png")))
        val tr = new AffineTransform()
        tr.scale(2,2)
        tr.rotate(math.atan2(ty-sy,tx-sx),image.getWidth()/2,image.getHeight()/2)
        val op = new AffineTransformOp(tr, AffineTransformOp.TYPE_BILINEAR)
        //g.setTransform(AffineTransform.getRotateInstance(Math.toRadians(45),image.getWidth()/2,image.getHeight()/2))
        g.drawImage(op.filter(image,null),position.x,position.y,null)
        //g.setTransform(backup)
    }

    /**
      * a fonction that paint the bullet in the right place
      *
      * @param g a Graphics2D object to draw with
      * @return whether the animation is finished or not
      */
    def paint(g: Graphics2D): Boolean =
    {
        if (target.distance(position) < 2*speed) return true
        drawBulllet(g)
        clock += 1
        return false
    }
}

class TowerBulletAnimation(val source:Point, val target:Point) extends TowerFireAnimation
{
    override val prefix: String = "/Bullet/1_"
}

class TowerMagicAnimation(val source:Point, val target:Point) extends TowerFireAnimation
{
    override val prefix: String = "/Magic/1_"
}

class TowerSlowAnimation(val source:Point, val target:Point) extends TowerFireAnimation
{
    override val prefix: String = "/Slow/1_"
}

class TowerRockAnimation(val source:Point, val target:Point) extends TowerFireAnimation
{
    override val prefix: String = "/Rock/1-"
    override val duration: Int = 58
    var impact = 0
    var didHit = false
    override def paint(g: Graphics2D): Boolean = 
    {
        if (target.distance(position) < 2*speed || didHit)
        {
            if (!didHit) {impact = clock; didHit = true}
            if (clock >= impact + 35) return true
            val image = TowerDefense.getImage("/Explosion/1_".concat(((clock-impact)/2).toString.concat(".png")))
            g.drawImage(image,target.x,target.y-30,null)
        }
        else drawBulllet(g)
        clock += 1
        return false
    }
}

class TowerBeamAnimation(val source:GameLogic.Towers.abstractTower, val target:GameLogic.Monsters.abstractMonster) extends Animation
{
    def paint(g: Graphics2D): Boolean = 
    {
        val tr = new AffineTransform()
        var image = TowerDefense.getImage("/laser.png")
        val tg = target.pos + GameLogic.Position(0.5,0.5)
        val src = source.pos + GameLogic.Position(0.5,0.2)
        val angle = math.atan2(tg.y - src.y, tg.x - src.x)
        tr.rotate(angle,image.getWidth()/2,image.getHeight()/2)
        tr.scale((tg -src).norm()*GridDisplay.MonsterDisplayer.cellsize.width/290,0.2)
        val op = new AffineTransformOp(tr, AffineTransformOp.TYPE_BILINEAR)
        image = op.filter(image,null)
        val drawX = ((if (src.x <= tg.x) src.x else tg.x)*GridDisplay.MonsterDisplayer.cellsize.width).toInt
        val drawY = ((if (src.y <= tg.y) src.y else tg.y)*GridDisplay.MonsterDisplayer.cellsize.height).toInt
        g.drawImage(image,drawX,drawY,null)
        return true
    }
}