package GUI
import swing._
import java.awt.Dimension
import java.awt.Color

import java.awt.CardLayout
import java.awt.image.BufferedImage

/**
  * Re-implementing a Panel with the Java CardLayout
  *
  */
class CardPanel extends Panel with LayoutContainer {
    type Constraints = String
    def layoutManager = peer.getLayout.asInstanceOf[CardLayout]
    override lazy val peer = new javax.swing.JPanel(new CardLayout) with SuperMixin
    
    /** the "stack of cards" that can be displayed */
    private var cards : Map[String, Component] = Map.empty
    
    /** useless here but maybe needed */
    protected def areValid(c: Constraints) = (true, "")
    
    /** add a new card (a Component) */ 
    protected def add(comp: Component, l: Constraints) = {
        // we need to remove previous components with the same constraints as the new one,
        // otherwise the layout manager loses track of the old one
        cards.get(l).foreach { old => cards -= l; peer.remove(old.peer) }
        cards += (l -> comp)
        peer.add(comp.peer, l)
    }
    
    /** show a spacific card */
    def show(l : Constraints) = layoutManager.show(peer, l)
      
    protected def constraintsFor(comp: Component) = cards.iterator.find { case (_, c) => c eq comp}.map(_._1).orNull
}

object TowerDefense extends SimpleSwingApplication
{
    javax.swing.UIManager.setLookAndFeel(new com.sun.java.swing.plaf.gtk.GTKLookAndFeel)
    var sprites = Map.empty[String,BufferedImage]
    val towersList = List(GameLogic.Towers.AOETower,GameLogic.Towers.Castle,GameLogic.Towers.Bardour, GameLogic.Towers.Wall,
        GameLogic.Towers.multiTower,GameLogic.Towers.slowTower)
    val monstersList = List(GameLogic.Monsters.Bird, GameLogic.Monsters.Boss, GameLogic.Monsters.Knight, GameLogic.Monsters.Mage,
        GameLogic.Monsters.Pegasus, GameLogic.Monsters.Unicorn)

    def getImage(path: String):BufferedImage =
    {
        if (!sprites.contains(path))
            sprites = sprites + (path -> javax.imageio.ImageIO.read(getClass.getResource(path)))
        return sprites(path)
    }

    val MainPanel = new CardPanel
    {
        /* add all the possible BorderFrame that could be used in the game */
        add(Interface.MainMenu,"MainMenu")
        add(Interface.Tutoriel,"Tutoriel")
        add(Interface.NotImplemented, "NotImplemented")
        add(Interface.GameInterface, "GameInterface")
        add(Interface.MapEditor,"MapEditor")
    }

    def exit(i:Int):Unit =
    {
        Interface.GameInterface.exitting = true
        Runtime.getRuntime().exit(i)
    }

    def top: Frame = new MainFrame
    {
        /* Put in a the MainFrame the CardPanel that "stacks" the other panels */
        preferredSize = new Dimension(1200,700)
        title = "TowerDefense"
        contents = MainPanel
        pack()
        centerOnScreen()
    }
}
