package GUI


package Interface

import java.awt.Color
import java.io.File
import GameLogic._
import swing._
import java.awt.Toolkit
import java.awt.image.BufferedImage
import scala.collection.mutable.Map

object MapEditorDisplayer extends Component
{
    preferredSize = new Dimension(1000, 500)
    var cellsize = new Dimension(0,0)

    override def paintComponent(g: Graphics2D):Unit =
    {
        /* get the size of the field */
        val size = g.getClipBounds().getSize()
        /* calculate the size of one cell */
        val celllength = math.min(size.width/MapEditor.Length, size.height/MapEditor.Height)
        cellsize = new Dimension(celllength,celllength)

        var image:BufferedImage = null
        

        for (x <- 0 to MapEditor.Length-1; y <- 0 to MapEditor.Height-1)
        {
            val imagename = MapEditor.tiles(x)(y) match {
                case GameLogic.Tiles.plain => "/grass1.png"
                case GameLogic.Tiles.mountain => "/Mountain.png"
                case GameLogic.Tiles.water => "/Water/0.png"
                case GameLogic.Tiles.fire => "/Fire.png"
                case GameLogic.Tiles.ice => "/Ice.png"
            }
            image = TowerDefense.getImage(imagename)
            g.drawImage(image, x*cellsize.width, y*cellsize.height, cellsize.width, cellsize.height, null, null)
        }

        g.setColor(Color.GREEN.darker().darker().darker())
        for (x <- 0 to MapEditor.Length) g.drawLine(x*cellsize.width,0,x*cellsize.width,MapEditor.Height*cellsize.height)
        for (y <- 0 to MapEditor.Height) g.drawLine(0,y*cellsize.height,MapEditor.Length*cellsize.width,y*cellsize.height)

        /* Draw the nexus */
        MapEditor.nexusPosition match {
            case Some(value) => image = TowerDefense.getImage("/nexus.png")
                g.drawImage(image,value.x.toInt*cellsize.width+1,value.y.toInt*cellsize.height+1,cellsize.width-1,cellsize.height-1,null,null)
            case None => ()
        }

        /* Draw the spawn points */
        for (c <- MapEditor.waves.keys)
        {
            image = if (c == MapEditor.currentPosition.getOrElse(Position(-1,-1))) TowerDefense.getImage("/selectvar.png")
            else TowerDefense.getImage("/select.png")
            g.drawImage(image,(c.x*cellsize.width).toInt,(c.y*cellsize.height).toInt,cellsize.width,cellsize.height,null,null)
        }

        Toolkit.getDefaultToolkit().sync() // Dunno what that does but its very important for performance
    }

    listenTo(mouse.clicks,mouse.moves)
    reactions +=
    {
        case ev @ (event.MouseDragged(_,_,_) | event.MouseClicked(_,_,_,_,_)) =>
        {
            val pt = ev.asInstanceOf[event.MouseEvent].point
            val x = pt.x/cellsize.width
            val y = pt.y/cellsize.height
            if (x < MapEditor.Length && y < MapEditor.Height)
            MapEditor.actionPickGroup.selected match {
                case Some(value) => value.text match {
                    case "Grass Tile" => MapEditor.updateTile(x, y, Tiles.plain)
                    case "Mountain Tile" => MapEditor.updateTile(x, y, Tiles.mountain)
                    case "Water Tile" => MapEditor.updateTile(x, y, Tiles.water)
                    case "Fire Tile" => MapEditor.updateTile(x, y, Tiles.fire)
                    case "Ice Tile" => MapEditor.updateTile(x, y, Tiles.ice)
                    case "Spawn Point" => MapEditor.addSpawnPoint(x, y)
                    case "Clear Spawn Point" => MapEditor.clearSpawnPoint(x, y)
                    case "Remove Spawn Point" => MapEditor.remSpawnPoint(x, y)
                    case "Nexus Position" => MapEditor.nexusPosition = Some(Position(x, y))
                }
                case None => ()
            }
        }
    }
}

object MapEditor extends BorderPanel
{
    var Length = 20
    var Height = 9
    var nexusPosition:Option[Position] = Some(Position(Length -1,Height/2))
    var tiles:Array[Array[Tiles.Value]] = Array.ofDim(Length,Height)
    for(x<-0 to Length-1;y<- 0 to Height-1) tiles(x)(y)=Tiles.plain
    var waves:Map[Position,Map[Int,List[(GameLogic.Monsters.abstractMonsterObject,Int)]]] = Map.empty
    var currentWave = 1
    var currentFile:Option[File] = None
    var currentPosition:Option[Position] = None

    /** The Quit game button */
    val exitButton = new Button(Action("Quit game")
    {
        TowerDefense.exit(0)
    })
    val mainButton = new Button(Action("Main Menu"){TowerDefense.MainPanel.show("MainMenu")})

    val lengthField = new TextField("20",3){action = (Action(""){updateLength(text.toInt)
        listenTo(mouse.clicks)
        reactions += {case event.MouseClicked(_,_,_,_,_) => selectAll()}})}
    val heightField = new TextField("9",3){action = (Action(""){updateHeight(text.toInt)
        listenTo(mouse.clicks)
        reactions += {case event.MouseClicked(_,_,_,_,_) => selectAll()}})}

    val fieldSizeChoice = new BoxPanel(Orientation.Vertical)
    {
        contents += new Label("Length/Height :")
        contents += new FlowPanel(lengthField,heightField)
    }

    val waveField = new TextField("1"){action = Action(""){updateWave(text.toInt)}}
    val waveChoice = new BoxPanel(Orientation.Horizontal)
    {
        contents += new Label("Wave n°")
        contents += waveField
    }

    val moneyField = new TextField("150",4)
    val moneyChoice = new FlowPanel(new Label("Starting Money : "),moneyField)

    val HPField = new TextField("50",4)
    val HPChoice = new FlowPanel(new Label("Nexus HP : "),HPField)

    val towerButtons = (for (t <- TowerDefense.towersList) yield (t -> new CheckMenuItem(t.name){maximumSize = new Dimension(1000,30);selected=true})).toMap
    val towersActiveMenu = new BoxPanel(Orientation.Vertical)
    {
        contents += new Label("The towers avaliable for this game")
        towerButtons.foreach(contents += _._2)
    }

    val grassButton = new RadioButton("Grass Tile")
    val mountainButton = new RadioButton("Mountain Tile")
    val waterButton = new RadioButton("Water Tile")
    val fireButton = new RadioButton("Fire Tile")
    val iceButton = new RadioButton("Ice Tile")
    val spawnButton = new RadioButton("Spawn Point")
    val remSpawnButton = new RadioButton("Remove Spawn Point")
    val clearSpawnButton = new RadioButton("Clear Spawn Point")
    val nexusButton = new RadioButton("Nexus Position")
    val actionPickGroup = new ButtonGroup(grassButton,mountainButton,waterButton,fireButton,iceButton,spawnButton,remSpawnButton,clearSpawnButton,nexusButton)

    val tilePickerMenu = new BoxPanel(Orientation.Vertical)
    {
        contents += new Label("Tool picker")
        contents += grassButton
        contents += mountainButton
        contents += waterButton
        contents += fireButton
        contents += iceButton
        contents += spawnButton
        contents += remSpawnButton
        contents += clearSpawnButton
        contents += nexusButton
    }

    val monstersComboBox = new ComboBox(TowerDefense.monstersList)
    val monstersField = new TextField("0", 4){horizontalAlignment = Alignment.Right
        listenTo(mouse.clicks)
        reactions += {case event.MouseClicked(_,_,_,_,_) => selectAll()}
        action = Action(""){spawnMonsterButton.doClick}
    }
    val spawnMonsterButton:Button = new Button(Action("Add monster"){waves(currentPosition.get)(currentWave) = 
        ((monstersComboBox.peer.getSelectedItem().asInstanceOf[GameLogic.Monsters.abstractMonsterObject],
        monstersField.text.toInt)::waves(currentPosition.get).getOrElse(currentWave,List.empty));monstersLabel.setText;repaint()}){enabled = false}
    val monstersLabel = new Label(){
        preferredSize = new Dimension(200,200)
        def setText():Unit = text = 
        """<html>
            <head>
            <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            td {
                text-align: center;
            }
            </style>
            </head>
            <body>
            <table style="width:100%">
            <tr>
                <th>Monster</th>
                <th>Spawn time</th>
            </tr>""" +
        (currentPosition match {
        case Some(value) => (for ((m,d) <- waves(value).getOrElse(currentWave,List.empty)) yield "<tr><td>" + m.name + "</td><td>" + d.toString+"</tr></td>").foldLeft("")((s1,s2) => s1+s2)
        case None => ""
    }) + "</table></body></html>"}
    monstersLabel.setText()

    val monstersPreviewBlock = new BoxPanel(Orientation.Vertical)
    {
        contents += new Label("Monsters at this spawn point :")
        contents += new ScrollPane(monstersLabel)
        contents += new FlowPanel(monstersComboBox,monstersField)
        contents += spawnMonsterButton
    }
    
    def updateLength(length: Int):Unit =
    {
        Length = length
        nexusPosition = Some(Position(Length -1,Height/2))
        currentPosition = None
        monstersLabel.setText()
        spawnMonsterButton.enabled = false
        tiles = Array.ofDim(Length,Height)
        for(x<-0 to Length-1;y<- 0 to Height-1) tiles(x)(y)=Tiles.plain
        waves = Map.empty
        repaint()
    }

    def updateHeight(height: Int):Unit =
    {
        Height = height
        nexusPosition = Some(Position(Length -1,Height/2))
        currentPosition = None
        monstersLabel.setText()
        spawnMonsterButton.enabled = false
        tiles = Array.ofDim(Length,Height)
        for(x<-0 to Length-1;y<- 0 to Height-1) tiles(x)(y)=Tiles.plain
        waves = Map.empty
        repaint()
    }

    def updateWave(wave: Int):Unit =
    {
        currentWave = wave
        currentPosition = None
        monstersLabel.setText()
        spawnMonsterButton.enabled = false
        repaint()
    }
    
    def updateTile(x: Int, y: Int, kind: Tiles.Value) = {tiles(x)(y) = kind;repaint()}

    def addSpawnPoint(x: Int, y: Int) = 
    {
        if (!waves.contains(Position(x,y)))
        waves(Position(x,y)) = Map.empty
        currentPosition = Some(Position(x,y))
        monstersLabel.setText()
        spawnMonsterButton.enabled = true
        repaint()
    }

    def clearSpawnPoint(x: Int, y: Int) =
    {
        waves(Position(x,y))(currentWave) = List.empty
        monstersLabel.setText()
    }

    def remSpawnPoint(x:Int, y:Int) = {waves -= Position(x,y)
        if (currentPosition == Some(Position(x,y))) {
            currentPosition = None
            monstersLabel.setText()
            spawnMonsterButton.enabled = false
        }
        repaint()
    }

    val reloadButton = new Button(Action("Reload"){this.repaint()})
    val loadButton = new Button(Action("Load a file")
    { 
        val f =new FileChooser(new File("./maps"))
        {
            preferredSize = new Dimension(200,100)
            title = "Choose a file"
        }
        if (f.showOpenDialog(this) == FileChooser.Result.Approve)
        {
            loadFile(f.selectedFile)
        }
    })

    val saveButton = new Button(Action("Save"){saveFile(currentFile.get)}){enabled = false}

    val saveAsButton = new Button(Action("Save As")
    {
        val f = new FileChooser(new File("./maps"))
        {
            preferredSize = new Dimension(200,100)
            title = "Save file as..."
        }
        if (f.showSaveDialog(this) == FileChooser.Result.Approve)
        {
            saveFile(f.selectedFile)
        }
    })

    def loadFile(file: File):Unit = 
    {
        val game = GameLogic.GameFromFile.fromString(io.Source.fromFile(file).mkString)
        moneyField.text = game.money.toString()
        towerButtons.foreach({case (_,m) => m.selected = false})
        game.availableTowers.foreach(id => towerButtons.find({case (t,_) => t.id == id}).get._2.selected = true)
        HPField.text = game.field.nexusHP.toString()
        nexusPosition = Some(game.field.nexusPosition)
        tiles = game.field.tiles
        Length = tiles.length
        lengthField.text = Length.toString()
        Height = tiles(0).length
        heightField.text = Height.toString()
        waves = Map.empty
        game.field.spawnPoints.foreach(p=> waves(p) = Map.empty)
        for(p<- 0 to game.field.spawnPoints.length -1; i<- 0 to game.waves.length-1)
            waves(game.field.spawnPoints(p))(i+1) = game.waves(i).contents.filter(_._2 == p).map(
                {case (a,b,c) => (TowerDefense.monstersList.find(_.id == a).get,c)})
        currentWave = 1
        currentFile = Some(file)
        saveButton.enabled = true
        currentPosition = None
        monstersLabel.setText()
        spawnMonsterButton.enabled = false
        repaint()
    }

    def saveFile(file : File):Unit =
    {
        def printPos(pos: Position, p: java.io.FileWriter):Unit =
            p.write(pos.x.toInt.toString + "." + pos.y.toInt.toString)
        currentFile = Some(file)
        saveButton.enabled = true
        val p = new java.io.FileWriter(file,false)
        p.write(moneyField.text.toInt + ";")
        towerButtons.filter(_._2.selected).map(_._1).foreach(t => p.write(t.id match {
            case Entities.aoe => "Ta"
            case Entities.bardour => "Tb"
            case Entities.castle => "Tc"
            case Entities.multi => "Tm"
            case Entities.slow => "Ts"
            case Entities.wall => "Tw"
        }))
        p.write(";")
        printPos(nexusPosition.get, p)
        p.write(",")
        p.write(HPField.text.toInt.toString)
        for (y <- 0 to Height -1)
        {
            p.write(",")
            for (x <- 0 to Length -1) p.write(tiles(x)(y) match {
            case Tiles.plain => "Cg"
            case Tiles.mountain => "Cm"
            case Tiles.water => "Cw"
            case Tiles.fire => "Cd"
            case Tiles.ice => "Cf"
        })}
        p.write(";")
        val spawnPoints = waves.keys.toList
        if (spawnPoints.nonEmpty)
        {
            printPos(spawnPoints.head, p)
            spawnPoints.tail.foreach(pos => {p.write(",");printPos(pos,p)})
        }
        p.write(";")
        val wavenumber = waves.keys.foldLeft(1)((m,p) => math.max(m,waves(p).keys.max))
            
        for (i<- 1 to wavenumber) {
            waves.foreach({case (pos,m) => m.getOrElse(i,List.empty).foreach({
                case (mo,d) => p.write(mo.id match {
                    case Entities.bird => "Mo"
                    case Entities.boss => "Mb"
                    case Entities.knight => "Mk"
                    case Entities.mage => "Mm"
                    case Entities.pegasus => "Mp"
                    case Entities.unicorn => "Mu"
                })
                p.write("." + spawnPoints.indexOf(pos) + "." + d + ",")
            })})
            p.write(";")
        }
        p.flush()
        p.close()
        repaint()
    }

    add(new BoxPanel(Orientation.Vertical)
    {
        contents += MapEditorDisplayer
        contents += new FlowPanel(new BoxPanel(Orientation.Vertical) {contents += fieldSizeChoice
            contents += new FlowPanel(waveChoice)
            contents += moneyChoice
            contents += HPChoice},towersActiveMenu)
    },BorderPanel.Position.Center)
    add(new BoxPanel(Orientation.Vertical)
    {
        contents += new FlowPanel(monstersPreviewBlock)
        contents += tilePickerMenu},BorderPanel.Position.East)
    add(new FlowPanel(reloadButton,saveButton,saveAsButton,loadButton,mainButton,exitButton),BorderPanel.Position.South)
}
