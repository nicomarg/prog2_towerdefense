package GUI
package Interface
import GameLogic._
import swing._
import java.awt.Toolkit
import java.awt.Color
import java.util.Timer
import java.util.TimerTask

class TowerButton(val kind: Towers.abstractTowerObject, text0: String) extends RadioButton
{
    text = text0
}

class MonsterLabel(val kind:Monsters.abstractMonsterObject) extends Label
{
    override def text_=(s: String): Unit = 
    {
        super.text_=(kind.name + " x"+s)
    }
}

/**
  * A panel do interact with the game
  */
object GameInterface extends BorderPanel
{  
    /** The game to interact with */
    var _game : abstractGame = GameFromFile("./maps/test")
    def game : abstractGame = _game
    def game_=(g: abstractGame):Unit = {
        _game = g
        GridDisplay.MonsterDisplayer.listenTo(g.field)
        towerButtons.contents.foreach(_.asInstanceOf[TowerButton].selected = false)
        towerButtons.contents.foreach(_.visible = false)
        for (t <- game.availableTowers) towerButtons.contents.foreach(b => if (b.asInstanceOf[TowerButton].kind.id == t)b.visible = true)
    }

    /** Lists the contents of the field at a given position */
    def fieldContents(p: Position):List[Entity] =
        game.field.contents.filter(_.pos == p)

    /** The last cell which was clicked */
    def lastPositionClicked : Position = GridDisplay.GridDisplayer.currentCell.getOrElse(null)
    var positionIsFree = true

    def createTowerButton(kind: Towers.abstractTowerObject):TowerButton =
        new TowerButton(kind,"Buy "+kind.name+" ("+kind.cost+" gold)")

    /** The Quit game button */
    val exitButton = new Button(Action("Quit game")
    {
        TowerDefense.exit(0)
    })
    val mainButton = new Button(Action("Main Menu"){TowerDefense.MainPanel.show("MainMenu")})

    /** A panel containing the tower buying buttons */
    val towerButtonGroup = new ButtonGroup()
    val towerButtons = new BoxPanel(Orientation.Vertical)
    {
        for (t <- TowerDefense.towersList) contents += (createTowerButton(t))
        for (b <- contents) towerButtonGroup.peer.add(b.asInstanceOf[TowerButton].peer)
    }

    /** The label displaying the current gold */
    val goldLabel = new Label
    /** The label displaying the current HP */
    val HPLabel = new Label

    val graphButton = new Button(Action("Toggle pathfinding graph")
    {
        GridDisplay.TowerDisplayer.displaygraph = !GridDisplay.TowerDisplayer.displaygraph
    })

    /** The panel containing hp, gold, ... */
    val playerStatus = new BoxPanel(Orientation.Vertical)
    {
        contents += goldLabel
        contents += HPLabel
        contents += graphButton
        contents += mainButton
        contents += exitButton
    }

    /** The button starting the next wave */
    val startwave = new Button(Action("Start the wave") {game.startRound()})
    /** The button skipping the next wave */
    val skipwave = new Button(Action("Skip wave") {gameTimer.purge();game.waveCounter+=1;game.mode=GameMode.preparation
        game.field.contents = game.field.contents.filter(!_.isInstanceOf[Monsters.abstractMonster]);GridDisplay.MonsterDisplayer.animations = List()})

    /** The label on top of the screen (displays wave count) */
    val topLabel = new Label

    val monsters = new BoxPanel(Orientation.Vertical)
    {
        for (m<- TowerDefense.monstersList; if game.waves.exists(_.contents.exists(_._1 == m.id))) contents += new MonsterLabel(m)
    }
    /** The panel showing the newt wave information */
    val monstersList = new BoxPanel(Orientation.Vertical)
    {
        contents += new Label("Monsters of the next wave :")
        contents += monsters
        contents += startwave
        contents += skipwave
    }

    def addTower(p:Position):Unit = 
    {
        val curentTower = towerButtonGroup.selected match {
            case Some(value) => value.asInstanceOf[TowerButton].kind
            case None => return
        }
        if (!game.field.canInsertTower(p) || game.money < curentTower.cost) return
        game.field.insertTower(curentTower.id, p)
        game.looseGold(curentTower.cost)
    }

    /** A function that updates the content of the labels and enables buttons */
    def updateHUD():Unit = 
    {
        startwave.enabled = game.mode == GameMode.preparation
        goldLabel.text = game.money.toString.concat(" Gold")
        HPLabel.text = game.field.nexusHP.toString.concat(" HP")
        topLabel.text = "Vague ".concat((game.waveCounter +1).toString.concat(" sur ".concat(game.waves.length.toString())))
        for (l <- monsters.contents) l.asInstanceOf[MonsterLabel].text = 
            game.waves(game.waveCounter).contents.filter(_._1 == l.asInstanceOf[MonsterLabel].kind.id).length.toString
    }
    def resetGame():Unit = 
    {
        game.money = game.baseMoney
        game.field.nexusHP = game.baseHP
        game.field.contents = List.empty
        game.waveCounter = 0
        game.field.mapGrid()
    }

    /** A boolean to tell the Timers to exit */
    var exitting = false
    /** A Timer object to refresh the display at a fixed rate */
    final def displayTimer = new Timer()
    /** The task that refreshes display */
    final def displayTask = new TimerTask
    {
        override def run(): Unit =
        {
            GridDisplay.MonsterDisplayer.repaint()
            if (exitting)
            {
                this.cancel()
                displayTimer.purge()
            }
        }
    }
    displayTimer.scheduleAtFixedRate(displayTask,500,10)

    /** A Timer to execute the game */
    final def gameTimer = new Timer()
    /** The task that does a time tick of the game */
    final def gameTask = new TimerTask
    {
        override def run(): Unit = 
        {
            if (game.mode == GameMode.fight) game.step()
            else 
            {
                gameTimer.purge()
                GridDisplay.MonsterDisplayer.animations = List()
            }
            if (game.waveCounter == game.waves.length)
            {
                new Dialog()
                {
                    resetGame()
                    title = "Victory !"
                    contents = new BorderPanel
                    {
                        add(new Label("GG, you won !"), BorderPanel.Position.Center)
                        add(new FlowPanel(
                            new Button (Action("Replay") 
                            {
                                close()
                            }),
                            new Button(Action("Main Menu")
                            {
                                TowerDefense.MainPanel.show("MainMenu")
                                close()
                            })), BorderPanel.Position.South)
                    }
                }.open()
            }
            else if (game.field.nexusHP <= 0)
            {
                resetGame()
                new Dialog()
                {
                    title = "Defeat !"
                    contents = new BorderPanel
                    {
                        add(new Label("You lost..."), BorderPanel.Position.Center)
                        add(new FlowPanel(
                            new Button (Action("Retry") 
                            {
                                close()
                            }),
                            new Button(Action("Main Menu")
                            {
                                TowerDefense.MainPanel.show("MainMenu")
                                close()
                            })), BorderPanel.Position.South)
                    }
                }.open()
            }
            updateHUD()
            if (exitting)
            {
                this.cancel()
                displayTimer.purge()
            }
        }
    }
    gameTimer.schedule(gameTask,500,100)

    /* Displaying the components */
    add(topLabel, BorderPanel.Position.North)
    add(new FlowPanel {
        contents += GridDisplay.GridDisplayer
    }, BorderPanel.Position.Center)
    add(new BorderPanel {
        add(monstersList,BorderPanel.Position.North)
        add(playerStatus,BorderPanel.Position.South)
    }, BorderPanel.Position.East)
    add(new FlowPanel(towerButtons), BorderPanel.Position.South)

    updateHUD()

    /** Updating the last clicked position according to the GridDisplayer signals */
    listenTo(GridDisplay.GridDisplayer)
    reactions +=
    {
        case GUI.GridDisplay.GridClicked(pos) =>
            {addTower(pos);updateHUD()}
    }
}
