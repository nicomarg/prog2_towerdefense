package GUI
package Interface
import swing._
import java.awt.Dimension
import java.awt.Color

/**
  * A small rule explaining panel
  */
object Tutoriel extends BorderPanel
{
      /* add a short explanation text and a button to go back to the main menu */
      add(new TextArea(){ editable = false
        text="Le but du jeu est defendre le \"nexus\" pour cela il faut poser des tours en dehors du chemin pris par les monstres \n Il y a donc 2 types de tours la 1iere est un petit chateau ne coutant pas tres cher et etant relativement peu efficace. La seconde est la \"Bardour\" beaucoup plus puissante et ayant un cout plus important. \n Il y a également 2 types de monstres, un chevalier peu puissant et un mage beaucoup plus fort"}, BorderPanel.Position.Center)
      add(new Button (Action("OK") {TowerDefense.MainPanel.show("MainMenu")} ), BorderPanel.Position.South)
}
/* For now it is a very simple tutorial, it will be improve in a real tutorial
 * in the futur */
