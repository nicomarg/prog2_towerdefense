package GUI
package Interface
import swing._
import java.awt.Dimension
import java.awt.Color

/**
  * A not implemented panel (you shouldn't see it)
  */
object NotImplemented extends BorderPanel
{

      /*add a sort text and a button to go back to the main menu */
      add(new Label("Sorry, not implemented yet"), BorderPanel.Position.Center)
      add(new Button (Action("OK") {TowerDefense.MainPanel.show("MainMenu")}), BorderPanel.Position.South)
}

/* Just a error message for the non-final version just in case */
