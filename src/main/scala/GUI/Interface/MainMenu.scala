package GUI
package Interface
import swing._
import java.awt.Dimension
import java.awt.Color
import java.io.File

/**
  * The panel which is the main menu
  */
object MainMenu extends BorderPanel
{
    /* Define the title */
    val topLabel = new Label("Welcome to TowerDefense")
    
    /* Define the button that link to the tutorial */
    val tutoButton = new Button(Action("Tutorial") {TowerDefense.MainPanel.show("Tutoriel")})
    
    /* Define the button that link to the main game*/
    val playButton = new Button(Action("Play the game"){TowerDefense.MainPanel.show("GameInterface")})

    val editorButton = new Button(Action("Map Editor"){TowerDefense.MainPanel.show("MapEditor")})

    val customButton = new Button(Action("Load a custom game"){
        val f =new FileChooser(new File("./maps"))
        {
            preferredSize = new Dimension(200,100)
            title = "Choose a file"
        }
        if (f.showOpenDialog(this) == FileChooser.Result.Approve)
        {
            GameInterface.game = GameLogic.GameFromFile(f.selectedFile.getCanonicalPath())
            TowerDefense.MainPanel.show("GameInterface")
        }
    })
    
    /* Define the button that allow to quit the game */
    val exitButton = new Button(Action("Quit game") {TowerDefense.exit(0)})
    
    /* Add the button into the Panel */
    add(topLabel, BorderPanel.Position.North)
    add(new FlowPanel {
        contents += tutoButton
        contents += playButton
        contents += customButton
        contents += editorButton
        preferredSize = new Dimension(200,50)
    }, BorderPanel.Position.Center)
    add(exitButton, BorderPanel.Position.South)
}

