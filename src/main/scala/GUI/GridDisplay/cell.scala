package GUI.GridDisplay
import swing._

/** 
 * Just a small class for a cell (ie a case where you can put a tower)
 * Not used atm
 */
abstract class Cell(val x:Int, val y:Int) extends Button
{
    background = java.awt.Color.WHITE
    preferredSize = new Dimension(50,50)
}
