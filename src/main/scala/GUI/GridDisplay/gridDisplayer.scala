package GUI

package GridDisplay
import swing._
import java.awt.Toolkit
import java.awt.Color
import java.awt.image.BufferedImage
import javax.swing.JLayeredPane

/** create a event for when a tile on the grid is clicked */
case class GridClicked(val pos:GameLogic.Position) extends event.Event

class Displayer extends Component
{
    def field = Interface.GameInterface.game.field
    preferredSize = new Dimension(1000, 500)
    var cellsize = new Dimension(0,0)
}

/**
  * The objects that display the grid with monsters and towers
  */
object GridDisplayer extends Displayer
{
    override lazy val peer = new JLayeredPane() with SuperMixin
    peer.add(FloorDisplayer.peer,JLayeredPane.DEFAULT_LAYER)
    peer.add(TowerDisplayer.peer,JLayeredPane.PALETTE_LAYER)
    peer.add(MonsterDisplayer.peer,JLayeredPane.MODAL_LAYER)
    peer.setBounds(0,0,preferredSize.width,preferredSize.height)
    FloorDisplayer.peer.setBounds(0,0,preferredSize.width,preferredSize.height)
    MonsterDisplayer.peer.setBounds(0,0,preferredSize.width,preferredSize.height)
    TowerDisplayer.peer.setBounds(0,0,preferredSize.width,preferredSize.height)
    peer.repaint()
    var currentCell:Option[GameLogic.Position] = None
    /* listen to the mouse to send a event when a case is click */
    listenTo(mouse.clicks,mouse.moves)
    reactions += {
        case e @ (event.MouseClicked(_,_,_,_,_) | event.MouseDragged(_,_,_)) =>
        {
            val p = e.asInstanceOf[event.MouseEvent].point
            if (p.x/cellsize.width < field.Length && p.y/cellsize.height < field.Height)
            {
                publish(GridClicked(GameLogic.Position(p.x/cellsize.width, p.y/cellsize.height)))
            }
        }
    }
    override def paintComponent(g: Graphics2D): Unit = 
    {
        /* get the size of the field */
        val size = g.getClipBounds().getSize()
        /* calculate the size of one cell */
        val celllength = math.min(size.width/field.Length, size.height/field.Height)
        cellsize = new Dimension(celllength,celllength)
        super.paintComponent(g)
    }
}

object FloorDisplayer extends Displayer
{
    var ticks = 0
    var clock = 0

    override def paintComponent(g: Graphics2D):Unit =
    {
        /* get the size of the field */
        val size = g.getClipBounds().getSize()
        /* calculate the size of one cell */
        val celllength = math.min(size.width/field.Length, size.height/field.Height)
        cellsize = new Dimension(celllength,celllength)

        ticks+= 1
        var image:BufferedImage = null
        

        for (x <- 0 to field.Length-1; y <- 0 to field.Height-1)
        {
            val imagename = field.tiles(x)(y) match {
                case GameLogic.Tiles.plain => (ticks/50+x+y/2)%3+1 match {
                    case 1 => "/grass1.png"
                    case 2 => "/grass2.png"
                    case 3 => "/grass3.png"
                    case 4 => "/grass2.png"
                }
                case GameLogic.Tiles.mountain => "/Mountain.png"
                case GameLogic.Tiles.water => ((ticks/80)%4) match {
                    case 0 => "/Water/0.png"
                    case 1 => "/Water/1.png"
                    case 2 => "/Water/2.png"
                    case 3 => "/Water/1.png"
                }
                case GameLogic.Tiles.fire => "/Fire.png"
                case GameLogic.Tiles.ice => "/Ice.png"
            }
            image = TowerDefense.getImage(imagename)
            g.drawImage(image, x*cellsize.width, y*cellsize.height, cellsize.width, cellsize.height, null, null)
        }

        /* Draw the spawn points */
        for (p <- field.spawnPoints)
        {
            image = TowerDefense.getImage("/select.png")
            g.drawImage(image, p.x.toInt*cellsize.width, p.y.toInt*cellsize.height,cellsize.width, cellsize.height,null,null)
        }

        /* Draw the nexus */
        image = TowerDefense.getImage("/nexus.png")
        g.drawImage(image,field.nexusPosition.x.toInt*cellsize.width+1,field.nexusPosition.y.toInt*cellsize.height+1,cellsize.width-1,cellsize.height-1,null,null)
        Toolkit.getDefaultToolkit().sync() // Dunno what that does but its very important for performance
    }
}

object TowerDisplayer extends Displayer
{
    var displaygraph = false
    override def paintComponent(g: Graphics2D):Unit =
    {
        /* get the size of the field */
        val size = g.getClipBounds().getSize()
        /* calculate the size of one cell */
        val celllength = math.min(size.width/field.Length, size.height/field.Height)
        cellsize = new Dimension(celllength,celllength)
        /* Draw the visibility graph */
        if (displaygraph)
        {
            for(x <- field.visibilityMap.keys)
            {
                g.fillOval(x.x.toInt*cellsize.width-5,x.y.toInt*cellsize.height-5,10,10)
                for (y <- field.visibilityMap(x))
                {
                    g.drawLine(x.x.toInt*cellsize.width,x.y.toInt*cellsize.height,y.x.toInt*cellsize.width,y.y.toInt*cellsize.height)
                }
            }
        }
        /* Draw the towers of the grid */
        for (e <- field.contents.filter(_.isInstanceOf[GameLogic.Towers.abstractTower]))
        {
            /* put a image on the cell corresponding on what is inside */
            val imagestr =
            e.id match {
                case GameLogic.Entities.bardour => "/bardour.png"
                case GameLogic.Entities.castle => "/castle.png"
                case GameLogic.Entities.multi => "/multi.png"
                case GameLogic.Entities.aoe => "/aoe.png"
                case GameLogic.Entities.wall => "/wall.png"
                case GameLogic.Entities.slow => "/slow.png"
            }
            val image = TowerDefense.getImage(imagestr)
            
            g.drawImage(image, (e.x*cellsize.width).toInt+1, (e.y*cellsize.height).toInt+1, cellsize.width-1, cellsize.height-1, null, null)
        }
        Toolkit.getDefaultToolkit().sync() // Dunno what that does but its very important for performance
    }

}

object MonsterDisplayer extends Displayer
{
    var animations:List[Animations.Animation] = List()
    override def paintComponent(g: Graphics2D):Unit =
    {
        /* get the size of the field */
        val size = g.getClipBounds().getSize()
        /* calculate the size of one cell */
        val celllength = math.min(size.width/field.Length, size.height/field.Height)
        cellsize = new Dimension(celllength,celllength)
        /* Draw the monsters of the grid */
        for (e <- field.contents.filter(_.isInstanceOf[GameLogic.Monsters.abstractMonster]))
        {
            /* put a image on the cell corresponding on what is inside */
            val imagestr =
            e.id match {
                case GameLogic.Entities.knight => "/knight.png"
                case GameLogic.Entities.mage => "/mage.png"
                case GameLogic.Entities.bird => "/bird.png"
                case GameLogic.Entities.unicorn => "/unicorn.png"
                case GameLogic.Entities.pegasus => "/pegasus.png"
                case GameLogic.Entities.boss => "/boss.png"
            }
            val image = TowerDefense.getImage(imagestr)
            
            g.drawImage(image, (e.x*cellsize.width).toInt+1, (e.y*cellsize.height).toInt+1, cellsize.width-1, cellsize.height-1, null, null)
            g.setColor(Color.black)
            g.drawRect(((e.x+0.1)*cellsize.width).toInt,((e.y+0.8)*cellsize.height).toInt,(0.8*cellsize.width).toInt,(0.2*cellsize.height).toInt)
            val prop = e.asInstanceOf[GameLogic.Monsters.abstractMonster].HP/e.asInstanceOf[GameLogic.Monsters.abstractMonster].baseStat.baseHP.toDouble
            g.setColor(if (prop <= 0.25) Color.red else if (prop <= 0.50) Color.orange else Color.green)
            g.fillRect(((e.x+0.1)*cellsize.width).toInt+1,((e.y+0.8)*cellsize.height).toInt+1,(0.8*cellsize.width*prop-2).toInt,(0.2*cellsize.height-2).toInt)
        }

        GridDisplayer.currentCell match {
            case Some(value) => g.drawImage(TowerDefense.getImage("/select.png"),
                (value.x*cellsize.width).toInt,(value.y*cellsize.height).toInt,cellsize.width,cellsize.height,null,null)
            case None => ()
        }
        

        for(a <- animations)
        {
            /* paint the animations and remove them if they are finished */
            if (a.paint(g)) animations.filter(_!=a)
        }
        Toolkit.getDefaultToolkit().sync() // Dunno what that does but its very important for performance
    }
    /* listen to the field to know when there is a new animation to start */
    listenTo(field)
    reactions += {
        case GameLogic.Towers.TowerFire(field, source, target, projectile) =>
            {
                val src = new Point (((source.x)*cellsize.width).toInt, ((source.y)*cellsize.height).toInt)
                val tg = new Point (((target.x)*cellsize.width).toInt, ((target.y)*cellsize.height).toInt)
                if (projectile != GameLogic.Projectiles.fire){
                animations = (projectile match {
                    case GameLogic.Projectiles.bullet => new Animations.TowerBulletAnimation(src,tg)
                    case GameLogic.Projectiles.magic => new Animations.TowerMagicAnimation(src,tg)
                    case GameLogic.Projectiles.rock => new Animations.TowerRockAnimation(src,tg)
                    case GameLogic.Projectiles.slow => new Animations.TowerSlowAnimation(src,tg)                    
                })::animations}
            }
        case GameLogic.Towers.MultiTarget(field, source, target) =>
            animations = (new Animations.TowerBeamAnimation(source, target))::animations
        case GameLogic.Towers.MultiUntarget(field, source, target) => animations = animations.filterNot(a => 
            if (a.isInstanceOf[Animations.TowerBeamAnimation]) (a.asInstanceOf[Animations.TowerBeamAnimation].source == source && 
            a.asInstanceOf[Animations.TowerBeamAnimation].target == target) else false)
        case GameLogic.Monsters.UnicornHealEvent(field, radius, pos) => animations = (new Animations.UnicornHealAnimation(radius,pos))::animations
        case GameLogic.Monsters.PegasusBornEvent(field, radius, monster) => animations = (new Animations.PegasusAuraAnimation(radius,monster))::animations
        case GameLogic.Monsters.MonsterRemoved(field, monster) => animations = animations.filterNot(a => 
            (a.isInstanceOf[Animations.TowerBeamAnimation] && a.asInstanceOf[Animations.TowerBeamAnimation].target == monster) ||
            (a.isInstanceOf[Animations.PegasusAuraAnimation] && a.asInstanceOf[Animations.PegasusAuraAnimation].source == monster))
    }
}
