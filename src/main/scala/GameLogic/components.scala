package GameLogic
import swing.Publisher

/** Enumeration for identifying Entities subclasses */
object Entities extends Enumeration
{
    val aTower, aMonster, bardour , castle, wall, multi, aoe, slow,
        knight, mage, bird, unicorn, pegasus, boss = Value
}

/** The different kind of floor tiles */
object Tiles extends Enumeration
{
    val water, mountain, ice, fire, plain = Value
}

/** Enumeration for identifying Projectiles subclasses */
object Projectiles extends Enumeration
{
    val bullet, magic, fire, rock, slow = Value
}
/**
  * Class for positions
  */
case class Position(val x:Double, val y:Double) extends Equals
{
    def canEqual(that:Any):Boolean = that.isInstanceOf[Position]

    override def equals(that:Any):Boolean =
        that match {
            case that:Position => that.x == x && that.y == y
            case _ => false
        }
    
    def +(that:Position):Position =
        Position(x+that.x,y+that.y)
    
    def -(that:Position):Position =
        Position(x-that.x,y-that.y)
    
    def *(that:Position):Double =
        x*that.y-y*that.x
    def *:(that:Double):Position =
        Position(x*that,y*that)
    
    def norm() = math.sqrt(x * x + y * y)
}

/**
  * Trait shared by anything that will be displayed on the field
  */
trait Drawable
{
    /** The coordinates */
    var pos:Position
    /** The x coordinate */
    def x:Double = pos.x
    /** The y coordinate */
    def y:Double = pos.y
}

/**
  * Class that regroups monsters and towers
  */
abstract class Entity extends Drawable with Publisher
{
    /** Implements the actions of an Entity */
    def act():Unit

    /** The identifier of the class */
    val id:Entities.Value
}

/**
  * Abstract type for damage
  */
abstract class abstractDamage
{
    /** The amount of damage */
    val value:Int
    val dmgType:Projectiles.Value
}

/**
  * Basic damage with a mandatory value
  *
  * @param value Sets the amount of damage
  * @param type Sets the projectiles type
  */
case class Damage(val value:Int, val dmgType:Projectiles.Value) extends abstractDamage
{
    
}

/**
  * Abstract type for effect
  */
abstract class Effect
{
    val duration:Int
}

/**
  * Basic slowness effect  with a mandatory value
  *
  * @param value Sets the decrese in speed
  * @param duration Sets the durations of the effect
  */
case class Slow(val value:Double, val duration:Int) extends Effect
{
    
}

/**
 * Basic Fire tick effect 
 *
 * @param value Sets the damage taken
 * @param duration Sets the durations of the effect
 */
case class Fire(val value:Int, val duration:Int) extends Effect
{

}

/** 
 *  Basic Fortune effect
 *  @param value set the increse in gold given by the mob
 *  @param duration Sets the durations of the effect
 *
 */
case class Fortune(val value:Int, val duration:Int) extends Effect
{

}

/** 
 *  Basic Increse_damage effect
 *
 *  @param value Sets the increse in damage taken by the mob
 *  @param duration Sets the durations of the effect
 *
*/
case class Damage_Boost(val value:Double, val duration:Int) extends Effect
{
  
}

/**
 *  Basic vulnerability effect
 *  @param value Sets the type of projectiles the mob is vulnerable to
 *  @param duration Sets the durations of the effect
 *
 */
case class Vulnerability(val value: Projectiles.Value, val duration:Int) extends Effect
{

}
