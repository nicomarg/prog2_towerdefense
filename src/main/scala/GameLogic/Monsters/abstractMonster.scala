package GameLogic
package Monsters

import scala.swing.event.Event
import scala.swing.Publisher

case class MonsterDeath(source:Publisher, died:abstractMonster) extends Event
case class MonsterRemoved(source:Publisher, died:abstractMonster) extends Event

/**
 *  Abstract base class for the companion objects of the Monsters
 */
/**
  * A class that regroups the stats of a monster
  *
  * @param speed The distance moved per tick of time
  * @param reward The amount of gold dropped
  * @param immun The damage immunities of the monster
  */
case class Stat (var speed:Double,var reward:Int,var immun: List[Projectiles.Value],var baseHP:Int)

abstract class abstractMonsterObject
{
    val baseStats:Stat
    val id:Entities.Value
    val name:String
    override def toString(): String = name
}

/** The abstract class of monsters */
abstract class abstractMonster extends Entity
{
    /** The class id, not very useful as this will be overridden by all the subclasses */
    val id = Entities.aMonster
    
    /** The hitpoints of the monster */
    var HP:Int = 0

    /** have the base stat of this type of Monster */
    var baseStat:Stat

    /** The amount of time units needed for the monster to move */
    def speed:Double = baseStat.speed/(effects.foldLeft(1.0)((e,p) =>
        p match
        {
            case (Slow(value,dure),_) => e*value
            case _ => e
        }))

    /** The path this monster will follow */
    private var _path:List[Position] = List()
    def path = _path
    def path_=(value:List[Position]) = _path = value

    /** The field the monster is on */
    var field:Field.abstractField

    /** The amount of gold dropped by the monster */
    def reward:Int = effects.foldLeft[Int](baseStat.reward)((e,p) => 
        p match
        {
            case (Fortune(value,dure),_) => return e+value
            case _ => return e
        })

    /** The projectiles the monster is immune to */
    def immun: List[Projectiles.Value] = baseStat.immun.filter(
        t => !effects.exists({ case (Vulnerability(value,_),_) => value == t
            case _ => false}))

    /** The effect the monster is currently effect by*/
    var effects:Map[Effect,Int] = Map()
    /**
      * Method called to make the monster take damage, and die if HP <= 0
      *
      * @param source the turret that inflicted the damage
      * @param dmg the damage object
      */
    def take_damage(source:Towers.abstractTower, dmg:abstractDamage) =
    {
        if (!(immun.contains(dmg.dmgType)))
        {
            HP -= ((effects.foldLeft(1.0)((e,p) => 
                p match {
                    case (Damage_Boost(value,dura),_) => e*value
                    case _ => e
                }))*dmg.value).toInt
            if (HP <= 0)
                death()
        }
    }

    def gainHP(value: Int) = HP = Math.min(HP + value,baseStat.baseHP)
    
    /** What to do when the monster dies */
    def death():Unit =
    {
        field.publish(MonsterDeath(field,this))
        field.remove(this)
    }

    var fireclock = 0
    
    /** Function that implements the actions (movement) of the monster
     * 
     * The action of a monster consist in moving every speed ticks of the clock
     */
    override def act():Unit =
    { 
        effects = effects.map( {case (k,v) => (k,v-1)})
        effects = effects.filter({ case (k,v) => v>= 0})
        effects.foreach[Unit]( {case (k,v) => 
            k match {
                case Fire(value,dura) =>
                    HP -= value; if (HP <= 0)
                        death()
                case _ => ()
            }})
        if (field.tiles(x.toInt)(y.toInt) == Tiles.fire)
        {
            fireclock += 1
            if (fireclock >= 2)
            {
                HP -= 1; if (HP <= 0)
                    death()
                fireclock = 0
            }
        }
        else fireclock = 0
        var rest = speed * (if (field.tiles((x+0.5).toInt)((y+0.5).toInt)==Tiles.ice) 0.5 else 1)
        while (path.nonEmpty && field.dist(pos,path.head)< rest)
        {
            pos = path.head
            rest = rest - field.dist(pos,path.head)
            path = path.tail
        }
        if (path.nonEmpty) move(path.head,rest)
        field.checkNexus(this)
    }

    def move(target:Position,distance:Double):Unit =
        pos = pos + Position((target.x-x)*distance/field.dist(target,pos),(target.y-y)*distance/field.dist(target,pos))
}
