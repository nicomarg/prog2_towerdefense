package GameLogic
package Monsters

object Mage extends abstractMonsterObject
{
    val baseStats = Stat(0.05,40,List(),50)
    val id = Entities.mage
    val name = "Mage"
}

/** a little more harder to beat Monster */
class Mage(var field:Field.abstractField,var pos:Position) extends abstractMonster
{
    override val id = Entities.mage
    var baseStat = Mage.baseStats
    HP = baseStat.baseHP
}
