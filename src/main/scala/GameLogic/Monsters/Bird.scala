package GameLogic
package Monsters

object Bird extends abstractMonsterObject
{
    val baseStats = Stat(0.1,5,List(Projectiles.rock),15)
    val id = Entities.bird
    val name = "Bird"
}

class Bird(var field:Field.abstractField,var pos:Position) extends abstractMonster
{
    var baseStat: Stat = Bird.baseStats
    HP = baseStat.baseHP
    override def path:List[Position] = List(field.nexusPosition)
    override def path_=(value:List[Position]):Unit = ()
    override val id: Entities.Value = Entities.bird
}