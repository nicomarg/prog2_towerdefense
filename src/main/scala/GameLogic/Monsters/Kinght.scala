package GameLogic
package Monsters

object Knight extends abstractMonsterObject
{
    val baseStats = Stat(0.1,10,List(),25)
    val id = Entities.knight
    val name = "Knight"
}
/** A simple monster that is quite simple to beat */
class Knight(var field:Field.abstractField, var pos:Position) extends abstractMonster
{
    override val id = Entities.knight
    var baseStat = Knight.baseStats
    HP = baseStat.baseHP
}
