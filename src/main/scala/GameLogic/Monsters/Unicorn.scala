package GameLogic
package Monsters

import GameLogic.Field.abstractField

case class UnicornHealEvent(val source:Field.abstractField, val radius:Double, val pos:Position) extends swing.event.Event

object Unicorn extends abstractMonsterObject
{
    val baseStats = Stat(0.05,0,List(Projectiles.bullet),150)
    val id = Entities.unicorn
    val name = "Unicorn"
}

class Unicorn(var field:Field.abstractField,var pos:Position) extends abstractMonster
{
    private var clock = 0
    var baseStat: Stat = Unicorn.baseStats
    HP = baseStat.baseHP
    override def act(): Unit = 
    {
        super.act()
        clock += 1
        if (clock >= 10)
        {
            field.publish(UnicornHealEvent(field, 5, pos))
            field.contents.filter(e => e.isInstanceOf[abstractMonster]
                && field.dist(e.pos,pos) <= 5).foreach[Unit](e => 
                    e.asInstanceOf[abstractMonster].gainHP(5))
            clock = 0
        }
    }
    override val id: Entities.Value = Entities.unicorn
}