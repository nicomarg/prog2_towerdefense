package GameLogic
package Monsters

case class PegasusBornEvent(val source:Field.abstractField, val radius: Double, val monster: Pegasus) extends swing.event.Event

object Pegasus extends abstractMonsterObject
{
    val baseStats = Stat(0.05,50,List(Projectiles.rock),100)
    val id = Entities.pegasus
    val name = "Pegasus"
}

class Pegasus(var field:Field.abstractField,var pos:Position) extends abstractMonster
{
    field.publish(PegasusBornEvent(field,5,this))
    override val id: Entities.Value = Entities.pegasus
    private var effect = Damage_Boost(0.7,5)
    var baseStat: Stat = Pegasus.baseStats
    HP = baseStat.baseHP
    override def act(): Unit = 
    {
        super.act()
        field.contents.filter(e => e.isInstanceOf[abstractMonster]
            && field.dist(e.pos,pos) <= 5).foreach[Unit](e => 
                e.asInstanceOf[abstractMonster].effects += 
                (effect -> effect.duration))
    }
}