package GameLogic
package Monsters

object Boss extends abstractMonsterObject
{
    val baseStats = Stat(0.05,300,List(),1000)
    val id = Entities.boss
    val name = "Boss"
}

class Boss(var field:Field.abstractField,var pos:Position) extends abstractMonster
{
    override val id: Entities.Value = Entities.boss
    private var clock = 0
    var baseStat: Stat = Boss.baseStats
    HP = baseStat.baseHP
    override def speed: Double = baseStat.speed
    override def act(): Unit = 
    {
        super.act()
        clock += 1
        if (clock >= 20)
        {
            for (x <- 1 to 5)field.insertMonster(Entities.knight,pos)
            for (_<-1 to 3)field.insertMonster(Entities.bird,pos)
            clock = 0
        }
    }
}
