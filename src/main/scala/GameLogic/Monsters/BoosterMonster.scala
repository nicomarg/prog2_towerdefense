package GameLogic
package Monsters

import scala.swing.event.Event
import scala.swing.Publisher

/** The abstract class of  Booster monsters */
abstract class BoosterMonster extends abstractMonster
{
    /** the effect the monster have */
    var monster_effect:Effect
    
    /** the range of effect */
    var range:Double

    /** Tells whether an entity is in range of the mob */
    def isInRange(e:Entity):Boolean =
      return (field.dist(pos, e.pos) <= range)

    override def act():Unit =
    { 
        field.contents.filter(e => ( e.isInstanceOf[Monsters.abstractMonster] && isInRange(e)
            )).asInstanceOf[List[Monsters.abstractMonster]].foreach(
            e => e.effects += (monster_effect -> monster_effect.duration))
        effects.map( {case (k,v) => (k,v-1)})
        effects = effects -- effects.filter({ case (k,v) => v<= 0 }).keys
        effects.foreach[Unit]( { case (k,v) =>
            k match {
            case Fire(value,dura) => HP -= value
            case _ => ()
        }})
        if (path.nonEmpty)
        {
            val target = path.head
            if (field.dist(pos,target)<= speed)
            {
                pos = Position(target.x,target.y)
                val rest = speed - field.dist(pos,target)
                path = path.tail
                if (path.nonEmpty) move(path.head,rest)
            }
            else move(target,speed)
        }
        field.checkNexus(this)
    }
}
