package GameLogic
import swing._
import java.awt.Dimension

/** a simple exemple of a possible game */
class midGame(override val field:Field.simpleField) extends abstractGame
{
    var waves = List(GameLogic.Waves.Wave12,GameLogic.Waves.Wave22,GameLogic.Waves.Wave32,GameLogic.Waves.Wave42)
    val baseMoney = 300
    money = baseMoney
}
