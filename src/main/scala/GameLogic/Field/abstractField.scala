package GameLogic
package Field

import scala.swing.Publisher
import scala.util.Random
import scala.collection.mutable._
import scala.math.Ordering.Implicits._

/**
  * A field on which entities will be located
  */
abstract class abstractField extends Publisher
{
    /** Length of the field */
    var Length:Int = 20

    /** Height of the field */
    var Height:Int = 9

    /** Topography of the field */
    var tiles:Array[Array[Tiles.Value]] = Array.ofDim(0,0)

    /** List of all the entities on the field */
    var contents:List[Entity] = List()
    
    /** The list of positions that make the path the monsters will follow */
    //var path:List[Position] = List()

    /** The hitpoints of the nexus */
    var nexusHP:Int

    /** The position of the nexus */
    val nexusPosition:Position

    /** The spawning points of the monsters */
    val spawnPoints:List[Position]

    /** A visibility graph of the game */
    var visibilityMap:Map[Position,List[Position]] = Map()
    /** The map of the distances in the visibility graph */
    var distMap:Map[Position,Double] = Map()
    /** The map of predecessors to follow shortest paths on the visibility graph */
    var predecessorMap:Map[Position,Position] = Map()
    /** The list of polygons (list of points in direct order, starting left) obstacles on the map*/
    var inaccessibleBlocs:List[List[Position]] = List()
    /** The list of the edges of the previous polygons */
    var blocEdges:List[Pair[Position,Position]] = List()

    /**
      * Finds the entities in the list that are adjacent to the given position
      */
    def findAdjacent(p:Position,positions:List[Position]):List[Position] =
    {
        val l = for (x <- -1 to 1; y <- -1 to 1; if x !=0 || y != 0) yield p+Position(x,y)
        return for {e<-positions.filter(p =>l.contains(p))} yield e
    }

    /**
      * Computes the set of the corners of the tile at the position
      */
    def tileCorners(p:Position):Set[Position] =
        Set(p,p+Position(0,1),p+Position(1,0),p+Position(1,1))

    /**
      * Computes the convex hull of a set of points
      * 
      * Computes an 'upper hull' and a 'lower hull' which are the upper and lower halfs of the hull
      * The product between p1 and p2 positions is defined by det(p1,p2)
      * It uses the fact that the input is a set of squares to compute a minimal convex hull
      */
    def convexHull(s:Set[Position]):List[Position] =
    {
        var points:List[Position] = s.toList.sortWith( (p1,p2) => (p1.x < p2.x) || (p1.x == p2.x && p1.y >= p2.y))
        var pointsUp = (points.head+Position(-1.0e-6,1.0e-5)) +: points :+ points.last+Position(1.0e-6,1.0e-5)
        var upperHull = List(pointsUp(1),pointsUp(0))
        for (n<- 2 to pointsUp.length-1)
        {
            // Checks if the next point makes the previous last useless or not
            
            while (upperHull.lengthCompare(2)>=0 && (upperHull(0)-upperHull(1))*(pointsUp(n)-upperHull(0))<=0)
                upperHull = upperHull.tail
            upperHull = pointsUp(n) :: upperHull
        }
        points = points.sortWith((p1,p2)=> (p1.x >= p2.x) || (p1.x == p2.x && p1.y <= p2.y))
        var pointsDown = (points.head+Position(1.0e-6,-1.0e-5)) +: points :+ points.last+Position(-1.0e-6,-1.0e-5)
        var lowerHull = List(pointsDown(1),pointsDown(0))
        for (n<- 2 to pointsDown.length-1)
        {
            while (lowerHull.lengthCompare(2)>=0 && (lowerHull(0)-lowerHull(1))*(pointsDown(n)-lowerHull(0))<=0)
                lowerHull = lowerHull.tail
            lowerHull = pointsDown(n) :: lowerHull
        }
        return upperHull.reverse.slice(1,upperHull.length-1) ++ lowerHull.reverse.slice(1,lowerHull.length-1)
    }

    /**
      * Creates the blocks that impede the monsters (turrets for now)
      * Isolates the groups of entities that will make a polygon, then computes the convex hull of this one
      */
    def createBlocs():Unit =
    {
        var unchecked = (for{x<- 0 to Length-1; y <- 0 to Height-1;if !(canMonsterWalk(Position(x,y)))} yield Position(x,y)).toList
        var polygon:Stack[Position] = Stack[Position]()
        var currentset:Set[Position] = Set()
        var blocks:List[Set[Position]] = List()
        while (unchecked.nonEmpty || polygon.nonEmpty)
        {
            if (polygon.isEmpty)
            {
                val e = unchecked.head
                unchecked = unchecked.tail
                blocks = blocks :+ currentset
                currentset = Set()
                polygon.push(e)
                currentset ++= tileCorners(polygon.head)
            }
            else
            {
                val l = findAdjacent(polygon.pop(),unchecked)
                for (p <- l)
                {
                    unchecked = unchecked.filter(_!=p)
                    polygon.push(p)
                    currentset ++= tileCorners(polygon.head)
                }
            }
        }
        blocks = blocks :+ currentset
        inaccessibleBlocs = blocks.filter(_.nonEmpty).map(convexHull)
    }

    def notEdgeOfMap(p1:Position,p2:Position):Boolean =
        p1.x > 0 && p2.x > 0 && p1.x < Length && p2.x < Length &&
        p1.y > 0 && p2.y > 0 && p1.y < Height && p2.y < Height

    /**
      * Creates the visibility graph of the grid
      * First, create the polygons, then list their edges.
      * Finally, checks for each pair of points if there is an edge blocking the path
      * This procedure is in O(n³) where n is tne number of vertices of the polygons.
      * It could be improvbed to O(n²log n) or even O(n²) but it would be way harder.
      * Furthermore, a relatively high complexity is ok as this will only be called when building a tower.
      */
    def mapGrid():Unit =
    {
        visibilityMap = Map()
        createBlocs()
        blocEdges = List[Pair[Position,Position]]()
        for (bloc <- inaccessibleBlocs)
            for (n<- 0 to bloc.length-1)
                blocEdges = (if (n== 0) Pair(bloc(bloc.length-1),bloc(n)) else Pair(bloc(n-1),bloc(n))) +: blocEdges
        var blocVertices = inaccessibleBlocs.foldLeft(List[Position]())((a,b)=>a++b)
        blocVertices = nexusPosition::spawnPoints++blocVertices
        for (x <- blocVertices)
        {
            var l = List[Position]()
            var prevx,postx = x
            var xOnBlock =
            inaccessibleBlocs.find(_.contains(x)) match {
                case None => false
                case Some(value) => val n = value.indexOf(x)
                    prevx = value((n-1+value.length)%value.length)
                    postx = value((n+1+value.length)%value.length)
                    true
            }
            if (notEdgeOfMap(prevx,x)) l = prevx::l
            if (notEdgeOfMap(postx,x)) l = postx::l
            for (y<- blocVertices)
            {
                var prevy,posty = y
                var yOnBlock =
                inaccessibleBlocs.find(_.contains(y)) match {
                    case None => false
                    case Some(value) => val n = value.indexOf(y)
                        prevy = value((n-1+value.length)%value.length)
                        posty = value((n+1+value.length)%value.length)
                        true
                }
                if (!inaccessibleBlocs.exists(l => l.contains(x) && l.contains(y)))
                if (!blocEdges.exists(e => ((y-x)*(e._1-y))*((y-x)*(e._2-y))<0 && ((e._2-e._1)*(x-e._2))*((e._2-e._1)*(y-e._2))<0))
                if (!yOnBlock || ((y-x)*(prevy-y))*((y-x)*(posty-y))>= 0)
                if (!xOnBlock || ((x-y)*(prevx-x))*((x-y)*(postx-x))>= 0)
                if (!blocVertices.exists(p => math.abs((p-x)*(p-y)) <= 1.0e-5 && dist(p,x) < dist (x,y) && dist(p,y) < dist(x,y)))
                    l = y::l
            }
            visibilityMap = visibilityMap + (x -> l)
        }
        dijkstra()
        for (e <- contents)
        if (e.isInstanceOf[Monsters.abstractMonster])
            e.asInstanceOf[Monsters.abstractMonster].path = clearPath(arbitraryPath(e.pos))
    }

    /**
      * An implementation of the dijkstra algorithm over the visibility graph
      */
    def dijkstra():Unit =
    {
        var queue = new PriorityQueue[Pair[Double,Position]]()(Ordering.by(_._1))
        distMap = (for {x<-visibilityMap.keys} yield (x,Double.PositiveInfinity))(collection.breakOut)
        predecessorMap = (for {x <- visibilityMap.keys} yield (x,null))(collection.breakOut)
        queue.enqueue((0,nexusPosition))
        predecessorMap(nexusPosition) = nexusPosition
        var done = Set[Position]()
        while (queue.nonEmpty)
        {
            val (d,pos) = queue.dequeue()
            if (!(done contains pos))
            for (x<- visibilityMap(pos))
            {
                if (d + dist(pos,x) < distMap(x))
                {
                    distMap(x) = d+dist(pos,x)
                    predecessorMap(x) = pos
                    queue.enqueue((d+dist(pos,x),x))
                }
            }
        }
    }

    /**
      * Computes the path from a point on the visibility graph to the nexus
      */
    def path(start:Position):List[Position] =
    {
        var l = List(start)
        var next = predecessorMap(start)
        while(next != nexusPosition)
        {
            l = next::l
            next = predecessorMap(next)
        }
        return (nexusPosition::l).reverse
    }

    /**
      * Computes the shortest path form any point to the nexus
      * This allows passing through an obstacle if one starts in this obstacle
      */
    def arbitraryPath(start:Position):List[Position] =
    {
        var l = List[Position]()
        for (x <- visibilityMap.keys)
        {
            if (!blocEdges.exists(e => ((x-start)*(e._1-x))*((x-start)*(e._2-x))<0 && ((e._2-e._1)*(start-e._2))*((e._2-e._1)*(x-e._2))<0))
            if (!visibilityMap.keys.exists(p => math.abs((p-start)*(p-x)) <= 1.0e-5 && dist(p,x) < dist (x,start) && dist(p,start) < dist(x,start)))
                l = x::l
        }
        var d = Double.PositiveInfinity
        var next = start
        for (x <- l)
        {
            val newd = dist(start,x) + distMap(x)
            if (newd < d)
            {
                d = newd
                next = x
            }
        }
        return start :: path(next)
    }

    def clearPath(l:List[Position]):List[Position] =
    l match {
        case Nil => Nil
        case head :: tl => (if (!canMonsterWalk(head)) head + Position (-1,-1)
            else if (!canMonsterWalk(head - Position(1,0))) head + Position(0,-1)
            else if (!canMonsterWalk(head - Position(0,1))) head + Position(-1,0)
            else head) :: clearPath(tl)
    }

    /**
      * Checks whether a monster has reached the nexus or not, and acts accordingly
      */
    def checkNexus(m:Monsters.abstractMonster):Unit =
    {
        if (dist(m.pos,nexusPosition) <= 0.5)
        {
            nexusHP -= 1
            remove(m)
        }
    }

    /** Returns the distance between the two positions */
    def dist(p1:Position, p2:Position):Double =
        return math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y))

    /**
      * Removes an Entity from the field
      *
      * @param e The entity that will be removed
      */
    def remove(e:Entity):Unit = 
    {
        if (e.isInstanceOf[Monsters.abstractMonster])
            publish(Monsters.MonsterRemoved(this,e.asInstanceOf[Monsters.abstractMonster]))
        contents = contents.filter(_!= e)
        for {e1 <- contents}
        {
            e1 match {
                case t:Towers.abstractTower => if (t.hasTarget(e)) t.removeTarget(e)
                case _ => ()
            }
        }
    }

    /** Make all the units on the field act for one tick of the clock */
    def actions():Unit =
    {
        for {e <- contents} e.act()
    }

    def canInsertTower(pos:Position):Boolean =
    {
        val t = new Towers.Castle(this,pos)
        contents = t::contents
        var b:Boolean = true

        for (p<- spawnPoints)
        {
            var visited:Array[Array[Boolean]] = ((for (x<-1 to Length) yield ((for (y<- 1 to Height) yield false)).toArray)).toArray
            b = b && DFS((p.x.toInt,p.y.toInt),visited)
        }
        contents = contents.tail
        return b && !contents.exists(t => t.isInstanceOf[Towers.abstractTower] && t.pos == pos) &&
        tiles(pos.x.toInt)(pos.y.toInt) != Tiles.water
    }

    def DFS(p:(Int,Int),visited:Array[Array[Boolean]]):Boolean =
    {
        if (dist(Position (p._1,p._2),nexusPosition)< 0.5) return true
        var b = false
        if ((p._1 >= 1) && !(visited(p._1-1)(p._2)) && canMonsterWalk(Position(p._1-1,p._2)))
        {
            visited(p._1-1)(p._2) = true
            b = b || DFS((p._1-1,p._2),visited)
        }
        if (p._1 <= Length-2 && !visited(p._1+1)(p._2) && canMonsterWalk(Position(p._1+1,p._2)))
        {
            visited(p._1+1)(p._2) = true
            b = b || DFS((p._1+1,p._2),visited)
        }
        if (p._2 >= 1 && !visited(p._1)(p._2-1) && canMonsterWalk(Position(p._1,p._2-1)))
        {
            visited(p._1)(p._2-1) = true
            b = b || DFS((p._1,p._2-1),visited)
        }
        if (p._2 <= Height-2 && !visited(p._1)(p._2+1) && canMonsterWalk(Position(p._1,p._2+1)))
        {
            visited(p._1)(p._2+1) = true
            b = b || DFS((p._1,p._2+1),visited)
        }
        return b
    }

    /**
      * Inserts an existing tower object on the field
      *
      * @param kind the Entities.Value corresponding to the kind of tower to be created
      * @param p the position where the tower will be inserted
      */
    def insertTower(kind:Entities.Value, p:Position):Unit =
    {
        var t:Towers.abstractTower = null
        kind match {
            case Entities.bardour => t = new Towers.Bardour(this, p)
            case Entities.castle => t = new Towers.Castle(this, p)
            case Entities.aoe => t = new Towers.AOETower(this,p)
            case Entities.slow => t = new Towers.slowTower(this,p)
            case Entities.wall => t = new Towers.Wall(this,p)
            case Entities.multi => t = new Towers.multiTower(this,p)
        }
        t.pos = p
        t.field = this
        contents = t::contents
        t.range *= 1.5
        mapGrid()
    }

    def canMonsterWalk(pos: Position):Boolean =
        pos.x < 0 || pos.x > Length -1 || pos.y < 0 || pos.y > Height -1 ||
        (!contents.exists(t => t.isInstanceOf[Towers.abstractTower] && t.pos == pos) &&
        tiles(pos.x.toInt)(pos.y.toInt)!= Tiles.water && tiles(pos.x.toInt)(pos.y.toInt) != Tiles.mountain)

    /**
      * Inserts an existing monster object on the field
      * 
      * The p parameter is optional, if not specified, puts the monster
      * at a spawn point.
      * @param kind the Entities.Value corresponding to the kind of monster to be created
      * @param s the index of spawn position where the monster will be inserted
      * @param p the position where the monster will be inserted
      */
    def insertMonster(kind:Entities.Value, p:Position):Unit =
    {
        var m:Monsters.abstractMonster = null
        kind match {
            case Entities.knight => m = new Monsters.Knight(this,p)
            case Entities.mage => m = new Monsters.Mage(this,p)
            case Entities.bird => m = new Monsters.Bird(this,p)
            case Entities.boss => m = new Monsters.Boss(this,p)
            case Entities.pegasus => m = new Monsters.Pegasus(this,p)
            case Entities.unicorn => m = new Monsters.Unicorn(this,p)
        }
        if (p.x < 0 || p.x > Length -1 || p.y < 0 || p.y > Height -1)
            m.pos = spawnPoints(0)
        contents = m::contents
        m.path = clearPath(arbitraryPath(m.pos))
    }
}
