package GameLogic
package Field

/** A simple field : the path is a horizontal line */
class simpleField extends abstractField
{
    /* a simple field  that juste a line in the middle for the monsters */
    val spawnPoints = List(Position(0,4))
    var nexusHP = 50
    val nexusPosition = Position(19,4)
    tiles =  (for {x <- 0 to Length-1}yield (for{y<- 0 to Height-1} yield Tiles.plain).toArray).toArray
    tiles(0)(4) = Tiles.fire
    tiles(1)(4) = Tiles.ice
    tiles(2)(4) = Tiles.mountain
    tiles(3)(4) = Tiles.water
    mapGrid()
}

