package GameLogic
package Towers

object AOETower extends abstractTowerObject
{
    var cost: Int = 100
    val id = Entities.aoe
    val name = "Catapult"
}

/** An AOE tower */
class AOETower(var field:Field.abstractField,var pos:Position) extends abstractTower
{
    var power: Int = 30
    var shootSpeed: Int = 15
    var range: Double = 15
    var target_number: Int = 1
    var impact_zone: Double = 2
    projectile = Projectiles.rock
    override val id: Entities.Value = Entities.aoe
}
