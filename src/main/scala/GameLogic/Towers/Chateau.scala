package GameLogic
package Towers

/** a simple compagnion that indicate the cost of the tower */
object Castle extends abstractTowerObject
{
    var cost = 25
    val id = Entities.castle
    val name = "Castle"
}

/** a cheap tower that is still quite effective */
class Castle(var field:Field.abstractField, var pos:Position) extends abstractTower
{
    override val id = Entities.castle
    var power = 2
    var shootSpeed = 6
    var range = 6
    var impact_zone = 0
    var slow = 0
    var target_number = 1
}
