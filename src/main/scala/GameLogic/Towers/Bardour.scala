package GameLogic
package Towers

/** 
 * a compagion object that fix the things that will not change about this tower
 * So the cost for now 
 */
object Bardour extends abstractTowerObject
{
    var cost = 100
    val id = Entities.bardour
    val name = "Bardour"
}

/** a new tower that is quite powerful */
class Bardour(var field:Field.abstractField, var pos:Position) extends abstractTower
{
    override val id = Entities.bardour
    var power = 10
    var shootSpeed = 10
    var range = 25
    var impact_zone = 0
    var slow: Int = 0
    var target_number: Int = 1
    projectile = Projectiles.magic
}
