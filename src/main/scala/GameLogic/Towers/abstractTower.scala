package GameLogic
package Towers

import scala.swing.Publisher
import scala.swing.event.Event

/**
  * Abstract base class for the companion objects of the towers
  */
abstract class abstractTowerObject
{
    /** The cost of the tower */
    var cost:Int

    /** The id of the tower */
    val id:Entities.Value

    /** The name of the tower */
    val name:String
}

case class TowerFire(val source:Publisher, val origin:abstractTower, val target:Monsters.abstractMonster, val projectile:Projectiles.Value) extends Event

/**
  * The abstract base class for towers
  */
abstract class abstractTower extends Entity
{
    /** The class id, not very useful as this will be overridden by all the subclasses */
    val id = Entities.aTower

    /** The numbers of ticks since last shot */
    private var clock:Int = 0

    /**The monster the tower is curently focusing */
    protected  var target:List[Monsters.abstractMonster] = List()

    /** Tells if the tower has an entity as target */
    def hasTarget(e:Entity):Boolean = (target.contains(e))

    /** Removes a target */
    def removeTarget(e:Entity):Unit = target = target.filter(_!= e)

    /** The field the tower is on */
    var field:Field.abstractField
    
    /** The amount of damage the turret deals */
    var power:Int
    
    /** The delay between shots */
    var shootSpeed:Int

    /** The range of the turret */
    var range:Double

    /** The number of target the tower may have **/
    var target_number:Int
    
    /** The type of projectiles the tower shot */
    var projectile = Projectiles.bullet

    /** the zone impacted by the shot (all mob in this area will take damage
     *  when the tower shot) */
    var impact_zone:Double
    /**
      * Hitting a monster
      *
      * @param m The target monster
      * @param k a attenuation factor 
      */
    def hit(m:Monsters.abstractMonster, k:Int):Unit =
    {
        m.take_damage(this, Damage(power/k, projectile))
    }

    /** Tells whether an entity is in range of the tower */
    def isInRange(e:Entity):Boolean =
        return (field.dist(pos, e.pos) <= range)

    /** Finds a new target if the previous one is unavaliable
     * 
     * Uses the filter method on the field contents to find a monster in range.
     * The asInstanceOf to cast the result is ok because the isInstanceOf test
     */
    private def findNewTarget():List[Monsters.abstractMonster] = 
        field.contents.filter(
            e => e.isInstanceOf[Monsters.abstractMonster] && isInRange(e) && !target.contains(e)
            ).asInstanceOf[List[Monsters.abstractMonster]]
    
    /** Function that finds the target the tower will hit
     * 
     * The tower keep the target it already has if possible, and if it has free
     * slots (the numbers of target is less that the number  possible) find new
     * target to aim.
     */
    def findTarget():List[Monsters.abstractMonster] =
    {
        target = target.filter(isInRange(_))
        if (target.length == target_number) target
        else
        {
            var newTargets = findNewTarget()
            target ++= newTargets.take(math.min(newTargets.length,target_number-target.length))
            target
        }
    }

    /** Function that implements the action (fire) of the tower
     * 
     * The action of a tower consist in firing every shootSpeed clock ticks
     * If there is no target avaliable, the tower waits with its shot ready
     */
    override def act():Unit =
    {
        clock += 1
        if (clock >= shootSpeed)
        {
            findTarget.foreach(e => {
                hit(e,1)
                field.publish(TowerFire(field,this,e,projectile))
                field.contents.filter( a => a.isInstanceOf[Monsters.abstractMonster] && 
                    (field.dist(a.pos,e.pos) <= impact_zone) && a!=e ).foreach(
                        a => hit(a.asInstanceOf[Monsters.abstractMonster],field.dist(a.pos,e.pos).toInt+1))
                })
            clock = 0
        }
    }
}

