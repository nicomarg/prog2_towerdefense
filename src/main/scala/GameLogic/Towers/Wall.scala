package GameLogic
package Towers

object Wall extends abstractTowerObject
{
    var cost: Int = 10
    val id = Entities.wall
    val name = "Wall"
}

class Wall(var field:Field.abstractField, var pos:Position) extends abstractTower
{
    override val id = Entities.wall
    var power = 0
    var shootSpeed = 0
    var range = 0
    var impact_zone = 0
    var target_number: Int = 0
}