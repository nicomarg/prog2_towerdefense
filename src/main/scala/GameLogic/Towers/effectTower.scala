package GameLogic
package Towers

import scala.swing.Publisher
import scala.swing.event.Event

abstract class effectTower extends abstractTower
{

    /** The type of effect created by the tower */
    var effect:Effect
 
    /**
      * Hitting a monster
      *
      * @param m The target monster
      * @param k the ratio (ignore here)
      */
    override def hit(m:Monsters.abstractMonster, k:Int):Unit =
    {
        m.effects = m.effects + (effect -> effect.duration)
    }
}    

