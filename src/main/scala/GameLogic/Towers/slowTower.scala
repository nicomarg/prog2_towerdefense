package GameLogic
package Towers

import GameLogic.Field.abstractField

object slowTower extends abstractTowerObject
{
    var cost: Int = 40
    var effect = Slow(2,15)
    val id = Entities.slow
    val name = "Slow"
}

/** A slowing tower */
class slowTower(var field: abstractField,var pos:Position) extends effectTower
{
    var power: Int = 0
    var shootSpeed: Int = 5
    var range: Double = 6
    var target_number: Int = Int.MaxValue
    var impact_zone: Double = 0
    var effect: Effect = slowTower.effect
    projectile = Projectiles.slow
    override val id: Entities.Value = Entities.slow
}
