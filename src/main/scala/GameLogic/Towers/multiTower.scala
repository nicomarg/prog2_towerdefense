package GameLogic

package Towers

case class MultiTarget(val source:scala.swing.Publisher, val origin:abstractTower, val target:Monsters.abstractMonster) extends scala.swing.event.Event
case class MultiUntarget(val source:scala.swing.Publisher, val origin:abstractTower, val target:Monsters.abstractMonster) extends scala.swing.event.Event

object multiTower extends abstractTowerObject
{
    var cost: Int = 50
    var effect = Fire(1,5)
    val id = Entities.multi
    val name = "Beam"
}

/** A multi-target tower that does continuous damage */
class multiTower(var field:Field.abstractField,var pos:Position) extends effectTower
{
    override def findTarget(): List[Monsters.abstractMonster] = 
    {
        val old_targets = target
        super.findTarget()
        target.filter(!old_targets.contains(_)).foreach[Unit](m => field.publish(MultiTarget(field,this,m)))
        old_targets.filter(!target.contains(_)).foreach[Unit](m => field.publish(MultiUntarget(field,this,m)))
        target
    }
    var power: Int = 0
    var shootSpeed: Int = 1
    var range: Double = 8
    var target_number: Int = 5
    var impact_zone: Double = 0
    projectile = Projectiles.fire
    var effect: Effect = multiTower.effect
    override val id: Entities.Value = Entities.multi
}