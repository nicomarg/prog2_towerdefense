package GameLogic
import swing._
import java.awt.Dimension

/** a simple exemple of a possible game */
class simpleGame(override val field:Field.simpleField) extends abstractGame
{
    var waves = List(GameLogic.Waves.Wave1,GameLogic.Waves.Wave2,GameLogic.Waves.Wave3,GameLogic.Waves.Wave4)
    val baseMoney = 150
    money = baseMoney
}
