package GameLogic
import swing._
import java.awt.Dimension

object GameMode extends Enumeration
{
    val preparation, fight = Value
}

/**
  * The abstract base class for games (sequence of waves on a field)
  */
abstract class abstractGame extends Reactor
{
    /** representation of the field with the towers and monsters */
    val field:Field.abstractField = new Field.simpleField()

    /** the internal timer since the beginning of the roud */
    var clockCounter:Int = 0

    /** number of waves already beaten by the player */
    var waveCounter:Int = 0

    /** the list of waves to do */
    var waves:List[Waves.Wave]

    val baseHP:Int = field.nexusHP
    val baseMoney:Int
    /** Amount of money the player has */
    var money:Int = baseMoney

    /** Liste of possible towers in this game */
    var availableTowers:List[Entities.Value] = List()
    
    /** fonction to add gold */
    def gainGold(award:Int):Unit =
    {
        money += award
    }
    
    /** fonction to loose gold */
    def looseGold(award:Int):Unit =
    {
        money -= award
    }

    /** 
     * Function that start a round, so reset the clock of the round and change
     * the mode 
     */
    def startRound():Unit =
    {
        clockCounter = 0
        mode = GameMode.fight
    }

    /** a step of the game */
    def step():Unit =
    {
        /* add the monsters that need to spawn on that turn */
        for(p <- waves(waveCounter).contents)
        {
            if (p._3 == clockCounter) field.insertMonster(p._1,field.spawnPoints(p._2))
        }
        /* the field make the action of the entities on it (towers that can
         * shot, shot and monsters that can move, move) */
        field.actions()
        clockCounter += 1
        /* check if the game is finish */
        if (field.nexusHP <= 0 || (!field.contents.exists(e => e.isInstanceOf[Monsters.abstractMonster])
            && waves(waveCounter).contents.forall(p => p._3 <= clockCounter)))
        {
            /* pass into the next wave and switch to preparation mode */ 
            mode = GameMode.preparation
            waveCounter += 1
        }
    }

    /** describe the part of the game currenly in place*/
    var mode:GameMode.Value = GameMode.preparation
    
    /* look for monsters death and add the amount of gold into the player
     * account */
    listenTo(field)
    
    reactions +=
    {
        case Monsters.MonsterDeath(f,m) => gainGold(m.reward)
    }
}
