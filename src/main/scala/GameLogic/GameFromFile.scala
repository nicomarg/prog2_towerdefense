package GameLogic
import swing._
import java.awt.Dimension

/** a simple exemple of a possible game */
class FieldFromFile(var terrain : Parser.Terrain, var spawnlist: List[Parser.Couple]) extends Field.abstractField
{
    var nexusHP = terrain.nex.hp
    val nexusPosition = Position(terrain.nex.pos.x,terrain.nex.pos.y)
    Height = terrain.cases.length
    Length = terrain.cases(0).length
    val tiles_temp = terrain.cases.map(_.map({
        case Parser.WATER => Tiles.water
        case Parser.MONTAGNE => Tiles.mountain
        case Parser.FREZZECASE => Tiles.ice
        case Parser.DPSCASE => Tiles.fire
        case Parser.GRASS => Tiles.plain
    }).toArray).toArray
    tiles = Array.ofDim(Length,Height)
    for (x <- 0 to Length -1; y <- 0 to Height -1) tiles(x)(y) = tiles_temp(y)(x)
    val spawnPoints = spawnlist.map(c=> c match{ case Parser.Couple(x,y) => Position(x,y)})
    mapGrid()
}
class yetAnotherGame(override val field : Field.abstractField, val baseMoney: Int) extends abstractGame
{
    var waves = List(GameLogic.Waves.Wave1,GameLogic.Waves.Wave2,GameLogic.Waves.Wave3,GameLogic.Waves.Wave4)
}

object GameFromFile
{
    def apply(ficher : String) : abstractGame = {
        fromString(io.Source.fromFile(ficher).mkString)
    }

    def fromString(s: String) : abstractGame =
    {
        var game : Parser.Game = Parser.FileToAst(s)
        var terter = new FieldFromFile(game.t,game.w.spawnPoints)
        new yetAnotherGame(terter, game.m.i){
            waves = game.w.waves.map(l => new Waves.Wave{contents = (l.map({
                case Parser.MonsterSpawn(Parser.BIRD,i,j) => (Entities.bird,i,j)
                case Parser.MonsterSpawn(Parser.BOSS,i,j) => (Entities.boss,i,j)
                case Parser.MonsterSpawn(Parser.KINGHT,i,j) => (Entities.knight,i,j)
                case Parser.MonsterSpawn(Parser.MAGE,i,j) => (Entities.mage,i,j)
                case Parser.MonsterSpawn(Parser.PEGASUS,i,j) => (Entities.pegasus,i,j)
                case Parser.MonsterSpawn(Parser.UNICORN,i,j) => (Entities.unicorn,i,j)
            }))})
            money = baseMoney
            availableTowers = game.tl.map({
                case Parser.AOETOWER => Entities.aoe
                case Parser.BARDOUR => Entities.bardour
                case Parser.CHATEAU => Entities.castle
                case Parser.MULTITOWER => Entities.multi
                case Parser.SLOWTOWER => Entities.slow
                case Parser.WALL => Entities.wall
            } )
        }
    }
}
