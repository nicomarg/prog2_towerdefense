package GameLogic
import swing._
import java.awt.Dimension

/** a simple exemple of a possible game */
class lateGame(override val field:Field.simpleField) extends abstractGame
{
    var waves = List(GameLogic.Waves.Wave13,GameLogic.Waves.Wave23,GameLogic.Waves.Wave33,GameLogic.Waves.Wave43)
    val baseMoney = 450
    money = baseMoney
}
