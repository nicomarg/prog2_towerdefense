package GameLogic
package Waves
import scala.io.Source
import java.util.Scanner

/** a series of waves (thery are quite easy) */
object Wave1 extends Wave
{
  contents = List(Pair(Entities.knight,0) , Pair(Entities.knight,0) ,Pair(Entities.knight,3), Pair(Entities.knight,5), Pair(Entities.knight, 8)).map({case (a,b) => (a,0,b)})

}

object Wave2 extends Wave
{
  contents = List(Pair(Entities.mage,0) , Pair(Entities.knight,0) ,Pair(Entities.knight,3), Pair(Entities.mage,5), Pair(Entities.knight, 8)).map({case (a,b) => (a,0,b)})

}

object Wave3 extends Wave
{
  contents = List(Pair(Entities.knight,0) , Pair(Entities.knight,0), Pair(Entities.knight,1), Pair(Entities.knight,2) ,Pair(Entities.knight,3), Pair(Entities.knight,4), Pair(Entities.mage,5), Pair(Entities.knight,5), Pair(Entities.knight,6), Pair(Entities.knight,7), Pair(Entities.knight, 8) ,Pair(Entities.knight,9) ,Pair(Entities.mage,10), Pair(Entities.knight,10)).map({case (a,b) => (a,0,b)})

}

object Wave4 extends Wave
{
  contents = List(Pair(Entities.mage,0) , Pair(Entities.mage,0) ,Pair(Entities.mage,3), Pair(Entities.mage,5), Pair(Entities.mage, 8) ,Pair(Entities.mage,8),Pair(Entities.mage,10),Pair(Entities.mage,15),Pair(Entities.mage,18),Pair(Entities.mage,20)).map({case (a,b) => (a,0,b)})

}

