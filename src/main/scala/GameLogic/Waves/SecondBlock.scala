package GameLogic
package Waves
import scala.io.Source
import java.util.Scanner

/** a series of waves (thery are quite easy) */
object Wave12 extends Wave
{
  contents = List(Pair(Entities.pegasus,0) , Pair(Entities.mage,0) ,Pair(Entities.knight,5), Pair(Entities.mage,7), Pair(Entities.unicorn, 10),Pair(Entities.mage,14)).map({case (a,b) => (a,0,b)})

}

object Wave22 extends Wave
{
  contents = List(Pair(Entities.mage,0) , Pair(Entities.unicorn,0) ,Pair(Entities.pegasus,4), Pair(Entities.mage,5), Pair(Entities.knight, 7),Pair(Entities.pegasus,10)).map({case (a,b) => (a,0,b)})

}

object Wave32 extends Wave
{
  contents = List(Pair(Entities.unicorn,0) , Pair(Entities.knight,0), Pair(Entities.unicorn,1), Pair(Entities.unicorn,3) ,Pair(Entities.unicorn,3), Pair(Entities.knight,4), Pair(Entities.mage,5), Pair(Entities.pegasus,8), Pair(Entities.pegasus,10), Pair(Entities.unicorn,12), Pair(Entities.unicorn,15) ,Pair(Entities.knight,18) ,Pair(Entities.pegasus,22), Pair(Entities.unicorn,27)).map({case (a,b) => (a,0,b)})

}

object Wave42 extends Wave
{
  contents = List(Pair(Entities.unicorn,0) , Pair(Entities.unicorn,0) ,Pair(Entities.pegasus,0), Pair(Entities.mage,8), Pair(Entities.unicorn, 10) ,Pair(Entities.mage,15),Pair(Entities.pegasus,20),Pair(Entities.unicorn,27),Pair(Entities.unicorn,30),Pair(Entities.pegasus,40)).map({case (a,b) => (a,0,b)})

}

