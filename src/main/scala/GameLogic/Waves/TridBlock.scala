package GameLogic
package Waves
import scala.io.Source
import java.util.Scanner

/** a series of waves (thery are quite easy) */
object Wave13 extends Wave
{
  contents = List(Pair(Entities.bird,0) , Pair(Entities.bird,0) ,Pair(Entities.bird,3), Pair(Entities.bird,5), Pair(Entities.bird, 8)).map({case (a,b) => (a,0,b)})


}

object Wave23 extends Wave
{
  contents = List(Pair(Entities.bird,0) , Pair(Entities.pegasus,0) ,Pair(Entities.bird,4), Pair(Entities.mage,6), Pair(Entities.boss, 20)).map({case (a,b) => (a,0,b)})

}

object Wave33 extends Wave
{
  contents = List(Pair(Entities.bird,0) , Pair(Entities.unicorn,0), Pair(Entities.pegasus,1), Pair(Entities.bird,2) ,Pair(Entities.unicorn,3), Pair(Entities.mage,4), Pair(Entities.unicorn,5), Pair(Entities.mage,5), Pair(Entities.knight,6), Pair(Entities.pegasus,7), Pair(Entities.pegasus, 8) ,Pair(Entities.unicorn,9) ,Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20), Pair(Entities.bird,20)).map({case (a,b) => (a,0,b)})

}

object Wave43 extends Wave
{
  contents = List(Pair(Entities.boss,0) , Pair(Entities.unicorn,0) ,Pair(Entities.mage,10), Pair(Entities.mage,10), Pair(Entities.pegasus, 18) ,Pair(Entities.bird,20),Pair(Entities.knight,22),Pair(Entities.pegasus,26),Pair(Entities.pegasus,32),Pair(Entities.pegasus,32),Pair(Entities.pegasus,32),Pair(Entities.pegasus,32),Pair(Entities.boss,50),Pair(Entities.boss,50)).map({case (a,b) => (a,0,b)})

}

