package Parser
import scala.util.parsing.combinator._
import scala.util.parsing.input._

sealed trait Token

case class Nombre(i : Int) extends Token
// Separateur
case object POINT extends Token // .
case object COMMA extends Token // ,
case object SEMICOLON extends Token // ;
// Cases
sealed trait Cases extends Token
case object WATER extends Cases // Cw
case object MONTAGNE extends Cases // Cm
case object DPSCASE extends Cases // Cd
case object FREZZECASE extends Cases // Cf
case object GRASS extends Cases // Cg
//Towers
sealed trait Towers extends Token
case object AOETOWER extends Towers // Ta
case object BARDOUR extends Towers // Tb
case object CHATEAU extends Towers // Tc
case object MULTITOWER extends Towers //Tm
case object SLOWTOWER extends Towers //Ts
case object WALL extends Towers //Tw
//Monsters
sealed trait Monsters extends Token
case object BIRD extends Monsters //Mo
case object BOSS extends Monsters //Mb
case object KINGHT extends Monsters //Mk
case object MAGE extends Monsters //Mm
case object PEGASUS extends Monsters //Mp
case object UNICORN extends Monsters //Mu

object Lexer  extends RegexParsers {
    //Delete all spaces.
    override def skipWhitespace = true
    override val whiteSpace = "[ \t\r\f\n]+".r

    def number : Parser[Nombre] = "0|[1-9][0-9]*".r ^^ {str => Nombre(str.toInt)}
    def comma = ","^^(_=> COMMA)
    def semiColon = ";"^^(_=> SEMICOLON)
    def point = "."^^(_=>POINT)
    //Cases
    def water = "Cw"^^(_=>WATER)
    def montagne = "Cm"^^(_=>MONTAGNE)
    def dpscase = "Cd"^^(_=>DPSCASE)
    def frezzecase = "Cf"^^(_=>FREZZECASE)
    def grass = "Cg"^^(_=>GRASS)
    //Towers
    def aoetower = "Ta"^^(_=>AOETOWER)
    def bardour = "Tb"^^(_=>BARDOUR)
    def chateau = "Tc"^^(_=>CHATEAU)
    def multitower = "Tm"^^(_=>MULTITOWER)
    def slowtower = "Ts"^^(_=>SLOWTOWER)
    def wall = "Tw"^^(_=>WALL)
    //Monsters
    def bird = "Mo"^^(_=>BIRD)
    def boss = "Mb"^^(_=>BOSS)
    def kinght = "Mk"^^(_=>KINGHT)
    def mage = "Mm"^^(_=>MAGE)
    def pegasus = "Mp"^^(_=>PEGASUS)
    def unicorn = "Mu"^^(_=>UNICORN)
    //Final parser
    def tokensParser : Parser[List[Token]] = {
      phrase(rep(number|comma|semiColon|point|water|montagne|dpscase|frezzecase|grass|aoetower|bardour|chateau|multitower|
        slowtower|wall|bird|boss|kinght|mage|pegasus|unicorn)) ^^ {l => l} }
    // Lexer fonction
    def apply(file: String): List[Token] = {
        parse(tokensParser,file) match {
            case Success(res,_) => res
            case Failure(str,next) => println(str); List[Token]()
            case Error(msg, next) => println(msg); List[Token]()
        }   
    }
}
       
class TokenReader(tokens : Seq[Token]) extends Reader[Token] {
    override def first : Token = tokens.head
    override def atEnd : Boolean = tokens.isEmpty
    override def pos : Position = NoPosition
    override def rest : Reader[Token] = new TokenReader(tokens.tail)
}

sealed trait FileAst
case class Money(i : Int) extends FileAst
case class Couple( x : Int, y : Int ) extends FileAst
case class MonsterSpawn(m : Monsters, spawn : Int, time : Int) extends FileAst
case class Waves( spawnPoints : List[Couple] , waves : List[List[MonsterSpawn]]) extends FileAst
case class Terrain(nex : Nexus, cases : List[List[Cases]]) extends FileAst
case class Nexus( pos : Couple, hp : Int ) extends FileAst
case class Game(m : Money,tl : List[Towers], t : Terrain, w : Waves) extends FileAst

object ParserAst extends Parsers {
    def repsepterm[T1, T2](p: Parser[T1],q: Parser[T2]) = (repsep(p,q) <~ (q?))
    def rep1septerm[T1, T2](p: Parser[T1], q: Parser[T2]) = (rep1sep(p,q) <~ (q?))
    override type Elem = Token
    private def nombre : Parser[Nombre] ={accept("nombre",{case n @ Nombre(nb) => n})}
    private def monsters : Parser[Monsters] ={accept("monstres",{ case m:Monsters => m })}
    private def towers : Parser[Towers] = {accept("towers",{ case t:Towers => t })}
    private def cases : Parser[Cases] = {accept("cases",{ case c:Cases => c })}
    def money:Parser[Money] = {accept("nombre", {case Nombre(nb) => Money(nb)})}
    def game:Parser[Game] = {phrase(money ~ SEMICOLON~tower_list ~SEMICOLON~ terrain~ SEMICOLON ~ waves) ^^ {case ms ~_~ tl~_ ~ t~_ ~ w => Game(ms,tl,t,w)}}
    def couple:Parser[Couple] = { (nombre~POINT~nombre)^^{case Nombre(x) ~_~ Nombre(y) => Couple(x,y)}}
    def monsterSpawn:Parser[MonsterSpawn] = {(monsters ~ POINT ~ nombre ~ POINT ~ nombre)^^{case m ~ _ ~ Nombre(x) ~_ ~ Nombre(y) => MonsterSpawn(m,x,y)}}
    def monsterSpawn_list : Parser[List[MonsterSpawn]] = rep1septerm(monsterSpawn,COMMA)
    def nexus:Parser[Nexus] = {(couple ~ COMMA ~  nombre)^^{case c ~ _ ~ Nombre(i) => Nexus(c,i)}}
    def tower_list:Parser[List[Towers]] = rep(towers)
    def cases_list:Parser[List[Cases]] = rep1(cases)
    def cases_list_list : Parser[List[List[Cases]]] = repsepterm(cases_list,COMMA)
    def spawn_list : Parser[List[Couple]] = repsepterm(couple,COMMA)
    def waves : Parser[Waves] = {(spawn_list~SEMICOLON~repsepterm(monsterSpawn_list,SEMICOLON))^^{case spawn ~ _ ~msl => Waves(spawn,msl)}}
    def terrain: Parser[Terrain] = {(nexus ~COMMA~cases_list_list)^^{case n~_~listcases => Terrain(n,listcases)}}
    
    def apply(tokens : Seq[Token]) : Game = {
        val reader = new TokenReader(tokens)
        game(reader) match {
            case Success(res,_) => res
            case Error(msg, next) => println(msg);throw new RuntimeException("Error while parsing file")
            case Failure(msg, next) => println(msg);throw new RuntimeException("Failure parsing the file")
        }
    }
}

object FileToAst { def apply(ficher : String) : Game = {ParserAst(Lexer(ficher))} }
