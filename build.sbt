val swing = "org.scala-lang.modules" %% "scala-swing" % "2.0.1"
lazy val root = (project in file(".")).
settings(
    name := "My Project",
    libraryDependencies += swing,
    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
    )
